﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;

namespace SelfAssessment.Web.Models.Account
{
    public class EmployeeModel
    {
        public int Id { get; set; }
        public int MasterDataUserId { get; set; }
        public string NIK { get; set; }
        public string EmployeeName { get; set; }
        public int? Gender { get; set; }
        public string Address { get; set; }
        public List<SelectListItem> KelurahanOptions { get; set; }
        public int? MasterDataKelurahanId { get; set; }
        public string Kota { get; set; }
        public string Kecamatan { get; set; }
        public string KodePos { get; set; }
        public string Email { get; set; }
        public string RegionOffice { get; set; }
        public string Jabatan { get; set; }
        public string Divisi { get; set; }
        public string ExtensionNumber { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime ModifyDate { get; set; }

        public List<SelectListItem> ConstructKelurahanOptions(List<Kelurahan.KelurahanModel> kelurahan)
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem { Value = "", Text = "Kelurahan", Selected = true });

            if (kelurahan != null) 
            {
                foreach (var single in kelurahan)
                {
                    result.Add(new SelectListItem { Value = single.Id.ToString(), Text = single.Kelurahan });
                }
            }
            
            return result;
        }
    }
}
