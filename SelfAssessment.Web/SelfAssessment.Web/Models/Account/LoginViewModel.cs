﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SelfAssessment.Web.Models.Account
{
    public class LoginViewModel
    {
        public string UserId { get; set; }
        public string Password { get; set; }
    }
}
