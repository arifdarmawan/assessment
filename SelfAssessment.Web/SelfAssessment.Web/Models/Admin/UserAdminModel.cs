﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;

namespace SelfAssessment.Web.Models.Admin
{
    public class UserAdminModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int UserRole { get; set; }
        public string RegionId { get; set; }
        public string Region { get; set; }
        public List<SelectListItem> RegionOptions { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime ModifyDate { get; set; }

        public List<SelectListItem> ConstructRegionOptions(List<Region.RegionModel> region)
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem { Value = "", Text = "Region", Selected = true });
            foreach (var single in region)
            {
                result.Add(new SelectListItem { Value = single.Region.ToString(), Text = single.Region });
            }

            return result;
        }
    }
}