﻿using System;

namespace SelfAssessment.Web.Models.Admin
{
    public class DateRange
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    public class ResultUserModel
    {
        public int MasterDataUserId { get; set; }
        public DateTime Tanggal { get; set; }
        public string NIK { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string RegionOffice { get; set; }
        public string Divisi { get; set; }
        public string Gender { get; set; }
        public string Address { get; set; }
        public string Kelurahan { get; set; }
        public string ColorCode { get; set; }
        public string JamMasuk { get; set; }
        public string JamPulang { get; set; }
        public string Q1 { get; set; }
        public string Q2 { get; set; }
        public string Q3 { get; set; }
        public string Q4 { get; set; }
        public string Q5 { get; set; }
        public string Q6 { get; set; }
        public string Q7 { get; set; }
        public string Q8 { get; set; }
        public string Q9 { get; set; }
        public string Q10 { get; set; }
        public string Q11 { get; set; }
        public string Q12 { get; set; }
        public string Q13 { get; set; }
        public string Q14 { get; set; }
        public string Q15 { get; set; }
        public string Q16 { get; set; }
        public string Q17 { get; set; }
        public string Q18 { get; set; }
    }

}