﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SelfAssessment.Web.Models.Admin
{
    public class ResultSurveyModel
    {
        public string Question { get; set; }
        public string Result { get; set; }
    }
}
