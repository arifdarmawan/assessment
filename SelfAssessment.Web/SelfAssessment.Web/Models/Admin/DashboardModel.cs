﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SelfAssessment.Web.Models.Admin
{
    public class DashboardModel
    {
        public int TotalWarnaMerah { get; set; }
        public int TotalWarnaKuning { get; set; }
        public int TotalWarnaHijau { get; set; }
    }
}