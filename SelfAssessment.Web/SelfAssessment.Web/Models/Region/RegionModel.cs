﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SelfAssessment.Web.Models.Region
{
    public class RegionModel
    {
        public string RegionId { get; set; }
        public string Region { get; set; }
    }
}
