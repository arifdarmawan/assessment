﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;

namespace SelfAssessment.Web.Models.Survey
{
    public class DataSurveyModel
    {
        // Personal Data
        public int MasterDataUserId { get; set; }
        public string NIK { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Region { get; set; }
        public int Gender { get; set; }
        public string Address { get; set; }
        public int MasterDataKelurahanId { get; set; }
        public List<SelectListItem> KotaKabupatenOptions { get; set; }
        public List<SelectListItem> KecamatanOptions { get; set; }
        public List<SelectListItem> KelurahanOptions { get; set; }
        public List<SelectListItem> KodePosOptions { get; set; }

        //Question
        public string Question1 { get; set; }
        public string Question2 { get; set; }
        public string Question3 { get; set; }
        public string Question4 { get; set; }
        public string Question5 { get; set; }
        public string Question6 { get; set; }
        public string Question7 { get; set; }
        public string Question8 { get; set; }
        public string Question9 { get; set; }
        public string Question10 { get; set; }
        public string Question11 { get; set; }
        public string Question12 { get; set; }
        public string Question13 { get; set; }
        public string Question14 { get; set; }
        public string Question15 { get; set; }
        public string Question16 { get; set; }
        public string Question17 { get; set; }
        public string Question18 { get; set; }
        public bool IsApproved { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime ModifyDate { get; set; }

        public List<SelectListItem> ConstructKotaKabupatenOptions(List<Kelurahan.KelurahanModel> kotakabupaten)
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem { Value = "", Text = "Kota/Kabupaten", Selected = true });
            foreach (var single in kotakabupaten)
            {
                result.Add(new SelectListItem { Value = single.Id.ToString(), Text = single.Kota });
            }

            return result;
        }

        public List<SelectListItem> ConstructKecamatanOptions(List<Kelurahan.KelurahanModel> kecamatan)
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem { Value = "", Text = "Kecamatan", Selected = true });
            foreach (var single in kecamatan)
            {
                result.Add(new SelectListItem { Value = single.Id.ToString(), Text = single.Kecamatan });
            }

            return result;
        }

        public List<SelectListItem> ConstructKelurahanOptions(List<Kelurahan.KelurahanModel> kelurahan)
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem { Value = "", Text = "Kelurahan", Selected = true });
            foreach (var single in kelurahan)
            {
                result.Add(new SelectListItem { Value = single.Id.ToString(), Text = single.Kelurahan });
            }

            return result;
        }

        public List<SelectListItem> ConstructKodePosOptions(List<Kelurahan.KelurahanModel> kodepos)
        {
            var result = new List<SelectListItem>();

            result.Add(new SelectListItem { Value = "", Text = "Kode Pos", Selected = true });
            foreach (var single in kodepos)
            {
                result.Add(new SelectListItem { Value = single.Id.ToString(), Text = single.KodePos });
            }

            return result;
        }
    }
}