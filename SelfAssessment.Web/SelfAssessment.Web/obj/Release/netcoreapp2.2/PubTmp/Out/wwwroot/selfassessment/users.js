﻿(function ($) {
    'use strict';

    var initialUser = (function () {
        let grid, lblUserId, id, userId, lblUserName, userName, password, lblRegion, regionId, btnAdd, userModal, userModalDelete, btnSubmit, btnClose, modalStatus, btnDelete;

        var initialDom = function () {
            grid = $('#grid-user');
            lblUserId = $('#lbl_UserId');
            id = $('#Id');
            userId = $('#UserId');
            lblUserName = $('#lbl_UserName');
            userName = $('#UserName');
            lblRegion = $('#lbl_Region');
            regionId = $('#RegionId');
            btnAdd = $('#btn-add-user');
            userModal = $('#user-modal');
            userModalDelete = $('#user-modal-delete');
            btnSubmit = $('#btn-submit-user');
            btnDelete = $('#btn-delete-user');
            btnClose = $('#btn-close-modal');
            password = 'admin.123';
        };

        var initialGridUser = function () {
            grid.DataTable({
                ajax: { url: SELFASSESSMENT.URLContext.GetUserAdmin, dataSrc: '' },
                searching: true,
                info: true,
                columns: [
                    { data: "id", visible: false },
                    { data: "userId", title: "User Id" },
                    { data: "userName", title: "User Name" },
                    { data: "region", title: "Region" },
                    { data: "userLevel", title: "Role" },
                    { data: "createdBy", title: "Created By" },
                    { data: "createdDate", title: "Created Date" },
                    {
                        data: null,
                        title: "Action",
                        width: 90,
                        className: "text-center",
                        createdCell: function (td, cellData, rowData, row, col) {
                            $(td).html('<div class="btn-group">'
                                + '<a href="#" class="btn btn-primary tbl_btn_edit"> <i class="glyphicon glyphicon-edit"></i></a>'
                                + '<a href="#" class="btn btn btn-danger tbl_btn_delete"> <i class="glyphicon glyphicon-trash"></i></a>'
                                + '</div>');

                            $(td).find('.tbl_btn_edit').click(function () {
                                modalStatus = 'Edit';
                                userModal.modal({ backdrop: 'static', keyboard: false });
                                userModal.find('#mdl-user-title').text('Edit User');
                                userModal.modal('show');

                                btnSubmit.find('span').text("Update");

                                lblUserId.css({ top: '-10px', padding: '4px', opacity: '1', visibility: 'inherit', });                           
                                lblUserName.css({ top: '-10px', padding: '4px', opacity: '1', visibility: 'inherit', });
                                lblRegion.css({ top: '-10px', padding: '4px', opacity: '1', visibility: 'inherit', });

                                id.val(rowData.id);
                                userId.val(rowData.userId);
                                userName.val(rowData.userName);
                                regionId.val(rowData.Region);
                                $('#RegionId option[value="' + rowData.region + '"]').attr('selected', true);
                                
                            });

                            $(td).find('.tbl_btn_delete').click(function () {
                                id.val(rowData.id);
                                userModalDelete.modal('show');
                            });
                        }
                    }
                ],
                columnDefs: [{
                    targets: 6, render: function (data) {
                        return moment(data).format('DD/MM/YYYY');
                    }
                }],
                "order": [[5, 'desc']],
                select: true,
                initComplete: function (settings, json) {
                    $('#grid-report').find('thead th').addClass('font-weight-bold');
                }
            });
        };

        var openModalAddUser = function () {
            btnAdd.click(function () {
                resetUserForm();
                userModal.modal({ backdrop: 'static', keyboard: false })  
                userModal.modal('show');
                userModal.find('#mdl-user-title').text('Add User');
                modalStatus = 'Add';
            });
        };

        var closeModalAddUser = function () {
            btnClose.click(function () {
                userModal.modal('hide');
            });
        };

        var submitUser = function () {
            btnSubmit.click(function (event) {
                event.preventDefault();
                btnSubmit.attr('disabled', true);
                btnSubmit.css('display', 'initial');
                SELFASSESSMENT.Utility.SubmitLoading(btnSubmit, 'Save');

                if (formValidation()) {

                    var userid = userId.val().toLowerCase();

                    let model = {
                        Id: id.val(),
                        UserId: userid,
                        UserName: userName.val(),
                        Password: SELFASSESSMENT.Utility.EncryptionHandler(password),
                        UserRole: '3',
                        RegionId: regionId.val(),
                        Region: regionId.val()
                    };
                    
                    if (modalStatus == 'Add') {

                        $.when(SELFASSESSMENT.Services.POSTLocal(model, SELFASSESSMENT.URLContext.SubmitUser)).done(function (result, status, xhr) {
                            if (result.code === 200) {
                                setTimeout(function () {
                                    btnSubmit.attr('disabled', false);

                                    grid.DataTable().ajax.reload();

                                    SELFASSESSMENT.Utility.SubmitRemoveLoading(btnSubmit, 'Save');
                                    SELFASSESSMENT.Utility.ConstructNotificationSuccess(result.message);

                                    userModal.modal('hide');
                                }, 500);
                            } else {
                                setTimeout(function () {
                                    btnSubmit.attr('disabled', false);

                                    SELFASSESSMENT.Utility.SubmitRemoveLoading(btnSubmit, 'Save');
                                    SELFASSESSMENT.Utility.ConstructNotificationError(result.message);
                                }, 500);
                            }
                        });

                    } else {

                        $.when(SELFASSESSMENT.Services.POSTLocal(model, SELFASSESSMENT.URLContext.UpdateUser)).done(function (result, status, xhr) {
                            if (result.code === 200) {
                                setTimeout(function () {
                                    btnSubmit.attr('disabled', false);

                                    grid.DataTable().ajax.reload();

                                    SELFASSESSMENT.Utility.SubmitRemoveLoading(btnSubmit, 'Save');
                                    SELFASSESSMENT.Utility.ConstructNotificationSuccess(result.message);

                                    userModal.modal('hide');
                                }, 500);
                            } else {
                                setTimeout(function () {
                                    btnSubmit.attr('disabled', false);

                                    SELFASSESSMENT.Utility.SubmitRemoveLoading(btnSubmit, 'Save');
                                    SELFASSESSMENT.Utility.ConstructNotificationError(result.message);
                                }, 500);
                            }
                        });
                    }

                } else {
                    setTimeout(function () {
                        btnSubmit.attr('disabled', false);
                        SELFASSESSMENT.Utility.SubmitRemoveLoading(btnSubmit, 'Save');
                    }, 500);
                }
            });
        };

        var deleteUser = function () {
            btnDelete.click(function (event) {
                $.when(SELFASSESSMENT.Services.GETLocal(SELFASSESSMENT.URLContext.DeleteUser + "/" + id.val())).done(function (result, status, xhr) {
                    if (result.code === 200) {
                        setTimeout(function () {

                            grid.DataTable().ajax.reload();

                            SELFASSESSMENT.Utility.SubmitRemoveLoading(btnDelete, 'Yes');
                            SELFASSESSMENT.Utility.ConstructNotificationSuccess(result.message);

                            userModalDelete.modal('hide');
                        }, 500);
                    } else {
                        setTimeout(function () {
                            btnDelete.attr('disabled', false);

                            SELFASSESSMENT.Utility.SubmitRemoveLoading(btnDelete, 'Yes');
                            SELFASSESSMENT.Utility.ConstructNotificationError(result.message);
                        }, 500);
                    }
                });
            });
        };

        var formValidation = function () {
            var isFormValid = false,
                isUserId = false,
                isUserName = false,
                isRegion = false;

            if (userId.val() == '') {
                userId.addClass('invalid');
                if ($('.errUserId').length == 0) {
                    userId.after('<div class="invalid-feedback errUserId">' + 'Please fill the required field.' + '</div>');
                    isUserId = false;
                }
            } else {
                userId.removeClass('invalid');
                $('.errUserId').remove();
                isUserId = true;
            }

            if (userName.val() == '') {
                userName.addClass('invalid');
                if ($('.errUserName').length == 0) {
                    userName.after('<div class="invalid-feedback errUserName">' + 'Please fill the required field.' + '</div>');
                    isUserName = false;
                }
            } else {
                userName.removeClass('invalid');
                $('.errUserName').remove();
                isUserName = true;
            }

            if (regionId.val() <= 0) {
                $('.err-select-Region').remove();
                regionId.addClass('invalid');
                if ($('.err-select-Region').length == 0) {
                    regionId.after('<div class="invalid-feedback err-select-Region">' + 'Please fill the required field.' + '</div>');
                    isRegion = false;
                }
            } else {
                regionId.removeClass('invalid');
                $('.err-select-Region').remove();
                isRegion = true;
            }

            if (isUserId && isUserName && isRegion) {
                isFormValid = true;
            } else {
                isFormValid = false;
            }

            return isFormValid;
        };

        var inputChangeRemoveErr = function () {
            userId.on('input', function () {
                if (this.value.length > 0) {
                    $(this).removeClass('invalid');
                    userId.next().remove();
                }
            });

            userName.on('input', function () {
                if (this.value.length > 0) {
                    $(this).removeClass('invalid');
                    userName.next().remove();
                }
            });

            regionId.change(function (e) {
                if (this.value !== '' || this.value !== null)
                    $('.err-select-Region').remove(); lblRegion.removeAttr("style");
            });
        };

        var disableSpaceInput = function () {
            userId.on({
                keydown: function (e) {
                    if (e.which === 32)
                        return false;
                },
                change: function () {
                    this.value = this.value.replace(/\s/g, "");
                }
            });
        }

        var resetUserForm = function () {
            userId.val('');
            userName.val('');
            regionId.val('');

            $('#RegionId option:selected').attr('selected', false);

            lblUserId.removeAttr("style");
            lblUserName.removeAttr("style");
            lblRegion.removeAttr("style");
        };

        var run = function () {
            initialDom();
            initialGridUser();
            openModalAddUser();
            closeModalAddUser();
            submitUser();
            deleteUser();
            disableSpaceInput();
            inputChangeRemoveErr();
        };

        return {
            run: run
        };
    })();

    initialUser.run();
})(jQuery);
