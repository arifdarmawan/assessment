﻿(function ($) {
    'use strict';

    var initialDashboard = (function () {
        let btnViewReport;

        var initialDom = function () {
            btnViewReport = $('.btn-view-report');
        };

        var viewReport = function () {
            btnViewReport.click(function () {
                SELFASSESSMENT.Utility.SubmitLoading(btnViewReport);
                window.location.href = SELFASSESSMENT.URLContext.Report;
            });
        };

        var run = function () {
            initialDom();
            viewReport();
        };

        return {
            run: run
        };
    })();

    initialDashboard.run();
})(jQuery);
