﻿(function ($) {
    'use strict';

    var initialSurvey = (function () {
        let masterdatauserid, masterDataKelurahanId, gender, question1, question2, question3, question4, question5, question6, question7, question8, question9, question10, question11,
            question12, question13, question14, question15, question16, question17, question18, isapproved, chkagree, modalterms, btnbackward, btnSubmit, loading;

        var initialDom = function () {
            masterdatauserid = $('#MasterDataUserId');
            masterDataKelurahanId = $('#MasterDataKelurahanId');
            gender = $('#Gender');
            isapproved = $('#IsApproved');
            chkagree = $('#chkAgree');
            modalterms = $('#modal-terms');
            btnbackward = $('#btn-backward');
            btnSubmit = $('#btn-submit-survey');
            loading = $('#loginLoading');
        };

        var getValueOnChanged = function () {

            $('input[name="question1"]:radio').change(function () {
                question1 = $('input[type=radio][name=question1]:checked').attr('id');
            });

            $('input[name="question2"]:radio').change(function () {
                question2 = $('input[type=radio][name=question2]:checked').attr('id');
            });

            $('input[name="question3"]:radio').change(function () {
                question3 = $('input[type=radio][name=question3]:checked').attr('id');
            });

            $('input[name="question4"]:radio').change(function () {
                question4 = $("input[name='question4']:checked").val();
            });

            $('input[name="question5"]:radio').change(function () {
                question5 = $('input[type=radio][name=question5]:checked').attr('id');
            });

            $('input[name="question6"]:radio').change(function () {
                question6 = $('input[type=radio][name=question6]:checked').attr('id');

            });

            $('input[name="question7"]:radio').change(function () {
                question7 = $('input[type=radio][name=question7]:checked').attr('id');
            });

            $('input[name="question8"]:radio').change(function () {
                question8 = $('input[type=radio][name=question8]:checked').attr('id');
            });

            $('input[name="question9"]:radio').change(function () {
                question9 = $('input[type=radio][name=question9]:checked').attr('id');
            });

            $('input[name="question10"]:radio').change(function () {
                question10 = $('input[type=radio][name=question10]:checked').attr('id');
            });

            $('input[name="question11"]:radio').change(function () {
                question11 = $('input[type=radio][name=question11]:checked').attr('id');
            });

            $('input[name="question12"]:radio').change(function () {
                question12 = $('input[type=radio][name=question12]:checked').attr('id');
            });

            $('input[name="question13"]:radio').change(function () {
                question13 = $('input[type=radio][name=question13]:checked').attr('id');
            });

            $('input[name="question14"]:radio').change(function () {
                question14 = $('input[type=radio][name=question14]:checked').attr('id');
            });

            $('input[name="question15"]:radio').change(function () {
                question15 = $('input[type=radio][name=question15]:checked').attr('id');
            });

            $('input[name="question16"]:radio').change(function () {
                question16 = $('input[type=radio][name=question16]:checked').attr('id');
            });

            $('input[name="question17"]:radio').change(function () {
                question17 = $('input[type=radio][name=question17]:checked').attr('id');
            });

            $('input[name="question18"]:radio').change(function () {
                question18 = $('input[type=radio][name=question18]:checked').attr('id');
            });
        };

        var formValidation = function () {
            var isAgree = false,
                isFormValid = false;

            if (!chkagree.is(":checked")) {
                if ($('#isAgree_err').length == 0) {
                    isAgree = false;
                }
            } else {
                $('#isAgree_err').remove();
                isapproved.val(true);
                isAgree = true;
            }

            if (isAgree) {
                isFormValid = true;
            }

            return isFormValid;
        };

        var chkIsApproved = function () {
            chkagree.click(function () {
                if ($(this).is(':checked')) {
                    btnSubmit.prop('hidden', false);
                    modalterms.modal('show');
                } else {
                    btnSubmit.prop('hidden', true);
                    modalterms.modal('hide');
                }
            });
        };

        var backwardEvent = function () {
            btnbackward.click(function () {
                btnSubmit.prop('hidden', true);
                chkagree.prop('checked', false);
            });
        };

        var submitSurvey = function () {
            btnSubmit.click(function (event) {
                event.preventDefault();
                btnSubmit.attr('disabled', true);
                btnSubmit.css('display', 'initial');
                SELFASSESSMENT.Utility.SubmitLoading(btnSubmit,'Submit');
               
                if (formValidation()) {
                   
                    let model = {
                        MasterDataUserId: masterdatauserid.val(),
                        Question1: question1,
                        Question2: question2,
                        Question3: question3,
                        Question4: question4,
                        Question5: question5,
                        Question6: question6,
                        Question7: question7,
                        Question8: question8,
                        Question9: question9,
                        Question10: question10,
                        Question11: question11,
                        Question12: question12,
                        Question13: question13,
                        Question14: question14,
                        Question15: question15,
                        Question16: question16,
                        Question17: question17,
                        Question18: question18,
                        IsApproved: isapproved.val()
                    };

                    $.when(SELFASSESSMENT.Services.POSTLocal(model, SELFASSESSMENT.URLContext.SubmitSurvey)).done(function (result, status, xhr) {
                        if (result.code === 200) {
                            setTimeout(function () {

                                window.location.href = SELFASSESSMENT.URLContext.Result;

                                SELFASSESSMENT.Utility.SubmitRemoveLoading(btnSubmit, 'Submit');
                                SELFASSESSMENT.Utility.ConstructNotificationSuccess(result.message);

                            }, 500);
                        } else {
                            setTimeout(function () {
                                btnSubmit.attr('disabled', false);
                                SELFASSESSMENT.Utility.SubmitRemoveLoading(btnSubmit, 'Submit');
                                SELFASSESSMENT.Utility.ConstructNotificationError(result.message);
                            }, 500);
                        }
                    });
                    
                } else {
                    setTimeout(function () {
                        btnSubmit.attr('disabled', false);
                        SELFASSESSMENT.Utility.SubmitRemoveLoading(btnSubmit, 'Submit');
                    }, 500);
                }
            });
        };

        var run = function () {
            initialDom();
            getValueOnChanged();
            chkIsApproved();
            backwardEvent();
            submitSurvey();
        };

        return {
            run: run
        };
    })();

    initialSurvey.run();
})(jQuery);