﻿/**
 * @namespace SELFASSESSMENT
 * 
 * */
var SELFASSESSMENT = SELFASSESSMENT || {};

/**
 * @module Version
 * 
 * */
SELFASSESSMENT.Version = '1.0.0';

/**
 * @module Global
 * 
 * */
SELFASSESSMENT.Global = {};
SELFASSESSMENT.Global.SetDropdownCaret = $('.select-wrapper.md-form.md-outline span.caret').css('z-index', '3');

/**
 * @module APIEndpoint
 * 
 * */
SELFASSESSMENT.ApiEndpoint = {};
SELFASSESSMENT.ApiEndpoint.BaseUrl = '';
SELFASSESSMENT.ApiEndpoint.DataSurveybyDateNow = '/api/Survey/GetDataSurveybyDateNow';

/**
 * @module URLContext
 * 
 * */
SELFASSESSMENT.URLContext = {};
SELFASSESSMENT.URLContext.Login = '/Account/Login';
SELFASSESSMENT.URLContext.UserAdd = '/User/Add';
SELFASSESSMENT.URLContext.UserUpdate = '/User/Update';
SELFASSESSMENT.URLContext.UpdateEmployee = '/User/UpdateEmployee';
SELFASSESSMENT.URLContext.Survey = '/Survey/Index';
SELFASSESSMENT.URLContext.GetDataKelurahanByCityId = '/User/GetDataKelurahanByCity';
SELFASSESSMENT.URLContext.GetDataKelurahanById = '/User/GetDataKelurahanById';
SELFASSESSMENT.URLContext.SubmitSurvey = '/Survey/SubmitData';
SELFASSESSMENT.URLContext.Result = '/Survey/Result';
SELFASSESSMENT.URLContext.Complete = '/Survey/Complete';
SELFASSESSMENT.URLContext.Dashboard = '/Admin/Dashboard';
SELFASSESSMENT.URLContext.Report = '/Admin/Report';
SELFASSESSMENT.URLContext.GetReportSurvey = '/Admin/GetReportSurvey';
SELFASSESSMENT.URLContext.GetReportSurveyByDateRange = '/Admin/GetReportSurveyByDateRange';
SELFASSESSMENT.URLContext.ReportDetail = '/Admin/ReportDetail';
SELFASSESSMENT.URLContext.User = '/Admin/Users';
SELFASSESSMENT.URLContext.GetUserAdmin = '/Admin/GetUserAdmin';
SELFASSESSMENT.URLContext.SubmitUser = '/Admin/SubmitUser';
SELFASSESSMENT.URLContext.UpdateUser = '/Admin/UpdateUser';
SELFASSESSMENT.URLContext.DeleteUser = '/Admin/DeleteUser';
SELFASSESSMENT.URLContext.SubmitChangePassword = '/Admin/SubmitChangePassword';


SELFASSESSMENT.GlobalMessage = {
    SubmitError: "Submit Error. Please check your form"
}

/* Get url APi */
$.get('/Home/GetUrlApiSelfAssessment', function (result, status, xhr) {
    SELFASSESSMENT.ApiEndpoint.BaseUrl = result;
});

/**
 * @module Services
 * 
 */
SELFASSESSMENT.Services = (function () {
    function get(urlContext) {
        var deferred = $.Deferred();

        $.ajax({
            type: "GET",
            url: SELFASSESSMENT.ApiEndpoint.BaseUrl + urlContext,
            success: function (result, status, xhr) {
                deferred.resolve(result);
            },
            error: function (result, status, xhr) {
                deferred.reject(result);
            }
        });

        return deferred.promise();
    };

    function getLocal(urlContext) {
        var deferred = $.Deferred();

        $.ajax({
            type: "GET",
            url: urlContext,
            success: function (result, status, xhr) {
                deferred.resolve(result);
            },
            error: function (result, status, xhr) {
                deferred.reject(result);
            }
        });

        return deferred.promise();
    };

    function postLocal(data, urlContext, contentType, processData) {
        var deferred = $.Deferred();

        $.ajax({
            type: "POST",
            data: data,
            url: urlContext,
            contentType: contentType !== 'undefined' ? contentType : 'application/x-www-form-urlencoded; charset=UTF-8',
            processData: processData !== 'undefined' ? processData : true,
            success: function (result, status, xhr) {
                deferred.resolve(result);
            },
            error: function (result, status, xhr) {
                deferred.reject(result);
            },
            timeout: 60000
        });

        return deferred.promise();
    }

    function putLocal(data, urlContext, queryString) {
        var deferred = $.Deferred();

        $.ajax({
            type: "PUT",
            data: data,
            url: urlContext + queryString,
            success: function (result, status, xhr) {
                deferred.resolve(result);
            },
            error: function (result, status, xhr) {
                deferred.reject(result);
            },
            timeout: 60000
        });

        return deferred.promise();
    }

    function deleteLocal(data, urlContext, queryString) {
        var deferred = $.Deferred();

        $.ajax({
            type: "DELETE",
            data: data,
            url: urlContext + queryString,
            success: function (result, status, xhr) {
                deferred.resolve(result);
            },
            error: function (result, status, xhr) {
                deferred.reject(result);
            },
            timeout: 60000
        });

        return deferred.promise();
    }

    function downloadFile(fileName) {
        var deferred = $.Deferred();
        console.log("Test");
        console.log(SELFASSESSMENT.URLContext.DownloadFile + '?fileName=' + fileName);

        $.ajax({
            url: SELFASSESSMENT.URLContext.DownloadFile + '?fileName=' + fileName,
            method: 'GET',
            success: function (result, status, xhr) {
                var base64str = result.data;

                // decode base64 string, remove space for IE compatibilitys
                var binary = atob(base64str.replace(/\s/g, ''));
                var len = binary.length;
                var buffer = new ArrayBuffer(len);
                var view = new Uint8Array(buffer);
                for (var i = 0; i < len; i++) {
                    view[i] = binary.charCodeAt(i);
                }

                // create the blob object with content-type "application/pdf"               
                var blob = new Blob([view], { type: "application/pdf" });
                var url = URL.createObjectURL(blob);

                deferred.resolve(url);
            },

            error: function (result, status, xhr) {
                deferred.reject(result);
            },
            timeout: 60000
        });

        return deferred.promise();
    }

    return {
        GET: get,
        GETLocal: getLocal,
        POSTLocal: postLocal,
        PUTLocal: putLocal,
        DELETELocal: deleteLocal,
        DownloadFile: downloadFile
    };
})();

/**
 * @module Utility
 * 
 * */
SELFASSESSMENT.Utility = (function () {
    function showLoading(obj) {
        obj.append('<span class="loading-animation"><i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw"></i></span>');
    };
    function removeLoading(obj) {
        obj.remove();
    };
    function constructDropdownOptions(elem, data) {
        var option = '<option value="" disabled selected>Select</option>';

        $.each(data, function (key, value) {
            option += "<option value='" + value.id + "'>" + value.name + "</option>";
        });

        elem.html(option);
    };
    function checkEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

        return regex.test(email);
    };
    function submitLoading(elem) {
        elem.html('<span class="glyphicon glyphicon-repeat gly-spin mr-1" role="status" aria-hidden="true"></span>Loading...').addClass('disabled');
    };
    function submitRemoveLoading(elem, title) {
        elem.find('span').remove();
        elem.removeClass('disabled');
        elem.text('Save');
        if (typeof title === 'undefined')
            elem.html('<i class="fa fa-save pr-2"></i><span>Save</span>');
        else
            elem.html('<i class="fa fa-save pr-2"></i><span>' + typeof title === 'undefined' ? 'Save' : title + '</span>');
    };
    function constructUpdateButton(elem) {
        elem.find('span').remove();
        elem.removeClass('disabled');
        elem.text('Update');
        elem.html('<i class="fa fa-edit pr-2"></i> <span>Update</span>');
    };
    function constructNotificationSuccess(msg) {
        toastr.options = {
            preventDuplicates: true,
            preventOpenDuplicates: true
        };
        toastr.success(msg, 'Success', { timeOut: 5000 });
    };
    function constructNotificationError(msg) {
        toastr.options = {
            preventDuplicates: true,
            preventOpenDuplicates: true
        };
        toastr.error(msg, 'Error', { timeOut: 5000 })
    };
    function encryptionHandler(msg) {
        var key = CryptoJS.enc.Utf8.parse('8080808080808080');
        var iv = CryptoJS.enc.Utf8.parse('8080808080808080');

        var encrypted = CryptoJS.AES.encrypt(
            CryptoJS.enc.Utf8.parse(msg),
            key,
            {
                keySize: 128 / 8,
                iv: iv,
                mode: CryptoJS.mode.CBC,
                padding: CryptoJS.pad.Pkcs7
            });

        return encrypted.toString();
    };
    return {
        ShowLoading: showLoading,
        RemoveLoading: removeLoading,
        ConstructDropdownOptions: constructDropdownOptions,
        CheckEmail: checkEmail,
        SubmitLoading: submitLoading,
        SubmitRemoveLoading: submitRemoveLoading,
        ConstructUpdateButton: constructUpdateButton,
        ConstructNotificationSuccess: constructNotificationSuccess,
        ConstructNotificationError: constructNotificationError,
        EncryptionHandler: encryptionHandler
    };
})();

/**
 * @module Cookie
 * */

SELFASSESSMENT.CookieConfiguration = (function () {
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires=" + d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    };

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    };

    return {
        SetCookie: setCookie,
        GetCookie: getCookie
    }
})();

/**
 * @module jQuery
 * 
 */
jQuery.fn.extend({
    configureMaterialSelect: function () {
        $(this).val("")
            .removeAttr('readonly').attr("placeholder", "Select ").prop('required', true)
            .addClass('form-control').css('background-color', '#fff');
    },
    configureMaterialSelectGroup: function () {
        $(this).val("")
            .removeAttr('readonly').attr("placeholder", "Select Max 5 Bussiness Field ").prop('required', true)
            .addClass('form-control').css('background-color', '#fff');
    },
    removeError: function () {
        $(this).on('change', function () {
            if ($(this).val() != '') {
                $(this).parent().next().next().remove();
            }
        });
    },
    constructUpdateButton: function () {
        $(this).find('span').remove();
        $(this).removeClass('disabled');
        $(this).text('Update');
        $(this).html('<i class="fa fa-edit pr-2"></i> <span>Update</span>');
    },
    constructSaveButton: function () {
        $(this).find('span').remove();
        $(this).removeClass('disabled');
        $(this).text('Save');
        $(this).html('<i class="fa fa-edit pr-2"></i> <span>Save</span>');
    },
    componentAddLoading: function () {
        $(this).html('<div class="d-flex justify-content-center">'
            + '<div class="spinner-grow text-dark" role="status">'
            + '<span class="sr-only">Loading...</span>'
            + '</div>'
            + '</div>');
    },
    iframeLoading: function () {
        $(this).html('<div class="spinner-grow text-dark" role="status" style="position: absolute; top: 50%; left: 50%;"><span class="sr-only">Loading...</span></div>');
    },
    titleTooltipOnApproved: function () {
        $(this).find('.vd_status_tooltip').remove('.vd_status_tooltip');
        $(this).append('<i class="fa fa-check-circle text-success pl-1 vd_status_tooltip" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Approved" data-original-title="Approved"></i>');
        $(this).find('.vd_status_tooltip').tooltip();
    },
    titleTooltipOnRejected: function () {
        $(this).find('.vd_status_tooltip').remove('.vd_status_tooltip');
        $(this).append('<i class="fa fa-times-circle text-danger pl-1 vd_status_tooltip" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Reject" data-original-title="Reject"></i>');
        $(this).find('.vd_status_tooltip').tooltip();
    },
    titleTooltipOnDisabled: function () {
        $(this).find('.vd_status_tooltip').remove('.vd_status_tooltip');
        $(this).append('<i class="fa fa-ban text-dark pl-1 vd_status_tooltip" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Disable" data-original-title="Disable"></i>');
        $(this).find('.vd_status_tooltip').tooltip();
    },
    titleTooltipOnPending: function () {
        $(this).find('.vd_status_tooltip').remove('.vd_status_tooltip');
        $(this).append('<i class="fa fa-hourglass-1 text-info pl-1 vd_status_tooltip" aria-hidden="true" data-toggle="tooltip" data-placement="bottom" title="Pending" data-original-title="Pending"></i>');
        $(this).find('.vd_status_tooltip').tooltip();
    },
    settingWrapCenter: function () {
        $(this).addClass('d-flex justify-content-center align-items-center mb-5');
    },
    resetWrapCenter: function () {
        $(this).removeClass('d-flex justify-content-center align-items-center mb-5');
    }
});




