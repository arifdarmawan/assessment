﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SelfAssessment.Web.Helper
{
    public enum CustomHttpStatusCode
    {
        UserAdNotRegistered = 600,
        EmployeeDataNotExist = 601,
        InsertFailDataAlreadyExist = 701
    }
}
