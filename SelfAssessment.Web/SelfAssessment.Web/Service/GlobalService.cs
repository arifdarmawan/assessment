﻿using Microsoft.Extensions.Configuration;
using SelfAssessment.Web.Helper;
using SelfAssessment.Web.Models.ResponseModel;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace SelfAssessment.Web.Service
{
    public class GlobalService 
    {
        HttpClient Client { get; }
        IConfiguration Configuration { get; }
        public GlobalService(HttpClient client, IConfiguration configuration)
        {
            Configuration = configuration;
            Client = client;
        }

        public async Task<BaseResponse> GetAsync(string uri) 
        {
            var result = new BaseResponse();

            try 
            {
                var response = await Client.GetAsync(uri);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            } 
            catch(Exception ex) 
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> PostAsync<T>(string uri, T model) 
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.PostAsync(uri, new JsonContent(model));

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }

            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> PostAsJsonAsync(string uri, string value)
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.PostAsJsonAsync(uri, value);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }

        public async Task<BaseResponse> PutAsync<T>(string uri, T model) where T : class
        {
            var result = new BaseResponse();

            try
            {
                var response = await Client.PutAsync(uri, model != null ? new JsonContent(model) : null);

                response.EnsureSuccessStatusCode();

                result = await response.Content.ReadAsAsync<BaseResponse>();
            }
            catch (Exception ex)
            {
                result.Message = ex.ToString();
            }

            return result;
        }
    }
}
