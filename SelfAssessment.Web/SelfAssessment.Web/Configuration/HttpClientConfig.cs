﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SelfAssessment.Web.Service;
using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace SelfAssessment.Web.Configuration
{
    public static class HttpClientConfig
    {
        public static IServiceCollection ManualSetSSL(this IServiceCollection services, IHostingEnvironment env, IConfiguration configuration) 
        {
            if (env.IsDevelopment())
            {
                services.AddHttpClient<GlobalService>("HttpClientWithSSLUntrusted", client =>
                {
                    client.BaseAddress = new Uri(configuration.GetValue<string>("SelfAssessmentAPIEndpoint:BaseUrl"));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                }).ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler
                {
                    ClientCertificateOptions= ClientCertificateOption.Manual,
                    ServerCertificateCustomValidationCallback = (httpRequestMessage, cert, cetChain, policyErrors) => { return true; }
                });
            }
            else {
                services.AddHttpClient<GlobalService>("HttpClientWithSSLUntrusted", client =>
                {
                    client.BaseAddress = new Uri(configuration.GetValue<string>("SelfAssessmentAPIEndpoint:BaseUrl"));
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(
                        new MediaTypeWithQualityHeaderValue("application/json"));
                });
            }

            return services;
        }
    }
}
