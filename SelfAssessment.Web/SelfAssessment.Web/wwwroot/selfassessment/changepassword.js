﻿(function ($) {
    'use strict';

    var initialUser = (function () {
        let oldPassword, newPassword, confirmPassword, btnChanged;

        var initialDom = function () {
            oldPassword = $('#OldPassword');
            newPassword = $('#NewPassword');
            confirmPassword = $('#ConfirmPassword');
            btnChanged = $('#btn-change-password');
        };

        var submitChangePassword = function () {
            btnChanged.click(function (event) {
                event.preventDefault();
                btnChanged.attr('disabled', true);
                btnChanged.css('display', 'initial');
                SELFASSESSMENT.Utility.SubmitLoading(btnChanged, 'Change Password');

                if (formValidation()) {
                    let model = {
                        OldPassword: SELFASSESSMENT.Utility.EncryptionHandler(oldPassword.val()),
                        ConfirmPassword: SELFASSESSMENT.Utility.EncryptionHandler(confirmPassword.val())
                    };
                   
                    $.when(SELFASSESSMENT.Services.POSTLocal(model, SELFASSESSMENT.URLContext.SubmitChangePassword)).done(function (result, status, xhr) {
                        if (result.code === 200) {
                            console.log("done");
                            setTimeout(function () {
                                btnChanged.attr('disabled', false);
                                SELFASSESSMENT.Utility.SubmitRemoveLoading(btnChanged, 'Change Password');
                                SELFASSESSMENT.Utility.ConstructNotificationSuccess(result.message);
                            }, 500);

                        } else {
                            console.log("error changes");
                            setTimeout(function () {
                                btnChanged.attr('disabled', false);
                                SELFASSESSMENT.Utility.SubmitRemoveLoading(btnChanged, 'Change Password');
                                SELFASSESSMENT.Utility.ConstructNotificationError(result.message);
                            }, 3000);

                        }
                    });

                } else {
                    setTimeout(function () {
                        btnChanged.attr('disabled', false);
                        SELFASSESSMENT.Utility.SubmitRemoveLoading(btnChanged, 'Change Password');
                    }, 500);
                }
            });
        }

        var formValidation = function () {
            var isFormValid = false,
                isoldPassword = false,
                isnewPassword = false,
                isconfirmPassword = false;

            if (oldPassword.val() == '') {
                oldPassword.addClass('invalid');
                if ($('.errOldPassword').length == 0) {
                    oldPassword.after('<div class="invalid-feedback errOldPassword">' + 'Please enter current password' + '</div>');
                    isoldPassword = false;
                }
            } else {
                oldPassword.removeClass('invalid');
                $('.errOldPassword').remove();
                isoldPassword = true;
            }

            if (newPassword.val() == '') {
                newPassword.addClass('invalid');
                if ($('.errNewPassword').length == 0) {
                    newPassword.after('<div class="invalid-feedback errNewPassword">' + 'Please enter new password' + '</div>');
                    isnewPassword = false;
                }
            } else {
                newPassword.removeClass('invalid');
                $('.errNewPassword').remove();
                isnewPassword = true;

                if (oldPassword.val() == newPassword.val()) {
                    newPassword.after('<div class="invalid-feedback errNewPassword">' + 'Old password and New Password cannot be same' + '</div>');
                    isnewPassword = false;
                }
            }

            if (confirmPassword.val() == '') {
                confirmPassword.addClass('invalid');
                if ($('.errConfirmPassword').length == 0) {
                    confirmPassword.after('<div class="invalid-feedback errConfirmPassword">' + 'Please enter confirm password' + '</div>');
                    isconfirmPassword = false;
                }
            } else {

                confirmPassword.removeClass('invalid');
                $('.errConfirmPassword').remove();
                isconfirmPassword = true;

                if (newPassword.val() != confirmPassword.val()) {
                    confirmPassword.after('<div class="invalid-feedback errConfirmPassword">' + 'Confirm password is not same as you new password' + '</div>');
                    isconfirmPassword = false;
                }
            }

            if (isoldPassword && isnewPassword && isconfirmPassword) {
                isFormValid = true;
            } else {
                isFormValid = false;
            }

            return isFormValid;
        }

        var inputChangeRemoveErr = function () {
            oldPassword.on('input', function () {
                if (this.value.length > 0) {
                    $(this).removeClass('invalid');
                    oldPassword.next().remove();
                }
            });

            newPassword.on('input', function () {
                if (this.value.length > 0) {
                    $(this).removeClass('invalid');
                    newPassword.next().remove();
                }
            });

            confirmPassword.on('input', function () {
                if (this.value.length > 0) {
                    $(this).removeClass('invalid');
                    confirmPassword.next().remove();
                }
            });
        };

        var run = function () {
            initialDom();
            submitChangePassword();
            inputChangeRemoveErr();
        };

        return {
            run: run
        };
    })();

    initialUser.run();
})(jQuery);