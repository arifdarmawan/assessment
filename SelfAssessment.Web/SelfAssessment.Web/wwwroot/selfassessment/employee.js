﻿(function ($) {
    'use strict';

    var initialEmployee = (function () {

        let masterdatauserid, address, lblkota, kota, lblkecamatan, kecamatan, lblkelurahan, masterdatakelurahanid,
            lblkodepos, kodepos, gender, radiopria, radiowanita, btnsave, btnupdate;

        var initialDom = function () {
            masterdatauserid = $('#MasterDataUserId');
            address = $('#Address');
            lblkota = $('#lbl_Kota');
            kota = $('#Kota');
            lblkecamatan = $('#lbl_Kecamatan');
            kecamatan = $('#Kecamatan');
            lblkelurahan = $('#lbl_kelurahan');
            masterdatakelurahanid = $('#MasterDataKelurahanId');
            lblkodepos = $('#lbl_KodePos');
            kodepos = $('#KodePos');
            gender = $('#Gender');
            radiopria = $('#radioPria');
            radiowanita = $('#radioWanita');
            btnsave = $('#btn-save-data-user');
            btnupdate = $('#btn-update-employee');
        };

        var getValueRadiogender = function () {
            if (gender.val() == 1) {
                radiopria.prop('checked', true);
                radiowanita.prop('checked', false);
            } else if (gender.val() == 2) {
                radiopria.prop('checked', false);
                radiowanita.prop('checked', true);
            }
        };

        var kelurahanOnchanged = function () {
            masterdatakelurahanid.change(function () {
                if (this.value !== "") {
                    $.when(SELFASSESSMENT.Services.GETLocal(SELFASSESSMENT.URLContext.GetDataKelurahanById + "/" + this.value)).done(function (result, status, xhr) {
                        setTimeout(function () {
                            var model = result.data;
                            console.log(model);

                            lblkelurahan.css({ top: '-10px', padding: '4px', opacity: '1', visibility: 'inherit', });

                            lblkota.css({ top: '-10px', padding: '4px', opacity: '1', visibility: 'inherit', });
                            kota.val(model.kota);

                            lblkecamatan.css({ top: '-10px', padding: '4px', opacity: '1', visibility: 'inherit', });
                            kecamatan.val(model.kecamatan);

                            lblkodepos.css({ top: '-10px', padding: '4px', opacity: '1', visibility: 'inherit', });
                            kodepos.val(model.kodepos);
                        }, 500);
                    });
                }
            });
        }

        var pageload = function () {
            if (masterdatakelurahanid.val() > 0 || gender > 0) {
                $.when(SELFASSESSMENT.Services.GETLocal(SELFASSESSMENT.URLContext.GetDataKelurahanById + "/" + masterdatakelurahanid.val())).done(function (result, status, xhr) {
                    setTimeout(function () {

                        var model = result.data;
                        console.log(model);

                        kota.val(model.kota);
                        kecamatan.val(model.kecamatan);
                        kodepos.val(model.kodepos);

                    }, 500);
                });
            }
        };

        var genderOnChanged = function () {
            $('input[name="gender"]:radio').change(function () {
                var temp = $('input[type=radio][name=gender]:checked').attr('id');

                if (temp == 'radioPria') {
                    gender.val('1');
                } else {
                    gender.val('2');
                }
            });
        };

        var formValidation = function () {
            var isFormValid = false,
                isAddress = false,
                isKelurahan = false,
                isGender;

            if (address.val() == '') {
                address.addClass('invalid');
                if ($('.errAddress').length == 0) {
                    address.after('<div class="invalid-feedback errAddress">' + 'Please fill the required field.' + '</div>');
                    isAddress = false;
                }
            } else {
                address.removeClass('invalid');
                $('.errAddress').remove();
                isAddress = true;
            }

            if (masterdatakelurahanid.val() <= 0) {
                $('.err-select-Kelurahan').remove();
                masterdatakelurahanid.addClass('invalid');
                if ($('.err-select-Kelurahan').length == 0) {
                    masterdatakelurahanid.after('<div class="invalid-feedback err-select-Kelurahan">' + 'Please fill the required field.' + '</div>');
                    isKelurahan = false;
                }
            } else {
                masterdatakelurahanid.removeClass('invalid');
                $('.err-select-Kelurahan').remove();
                isKelurahan = true;
            }

            if (gender.val() <= 0) {
                $('.errGender').remove();
                gender.addClass('invalid');
                if ($('.errGender').length == 0) {
                    gender.after('<div class="invalid-feedback errGender">' + 'Please fill the required field.' + '</div>');
                    isGender = false;
                }
            } else {
                gender.removeClass('invalid');
                $('.errGender').remove();
                isGender = true;
            }

            if (isAddress && isKelurahan && isGender) {
                isFormValid = true;
            } else {
                isFormValid = false;
            }

            return isFormValid;
        };

        var saveEmployee = function () {
            btnsave.click(function () {
                SELFASSESSMENT.Utility.SubmitLoading(btnsave);
                if (formValidation()) {
                    let model = {
                        MasterDataUserId: masterdatauserid.val(),
                        Gender: gender.val(),
                        Address: address.val(),
                        MasterDataKelurahanId: masterdatakelurahanid.val()
                    };
                    $.when(SELFASSESSMENT.Services.POSTLocal(model, SELFASSESSMENT.URLContext.UpdateEmployee)).done(function (result, status, xhr) {
                        if (result.code === 200) {
                            setTimeout(function () {

                                window.location.href = SELFASSESSMENT.URLContext.Survey;
                                btnsave.constructUpdateButton();
                                SELFASSESSMENT.Utility.ConstructNotificationSuccess(result.message);

                            }, 500);
                        } else {
                            setTimeout(function () {
                                SELFASSESSMENT.Utility.SubmitRemoveLoading(btnsave);
                                SELFASSESSMENT.Utility.ConstructNotificationError(result.message);
                            }, 500);
                        }
                    });
                } else {
                    setTimeout(function () {
                        SELFASSESSMENT.Utility.SubmitRemoveLoading(btnsave, 'Save');
                    }, 300);
                }
            });
        };

        var updateEmployee = function () {
            btnupdate.click(function () {
                SELFASSESSMENT.Utility.SubmitLoading(btnupdate);
                if (formValidation()) {
                    let model = {
                        MasterDataUserId: masterdatauserid.val(),
                        Gender: gender.val(),
                        Address: address.val(),
                        MasterDataKelurahanId: masterdatakelurahanid.val()
                    };

                    $.when(SELFASSESSMENT.Services.POSTLocal(model, SELFASSESSMENT.URLContext.UpdateEmployee)).done(function (result, status, xhr) {
                        if (result.code === 200) {
                            setTimeout(function () {
                                btnupdate.constructUpdateButton();
                                SELFASSESSMENT.Utility.ConstructNotificationSuccess(result.message);

                            }, 500);
                        } else {
                            setTimeout(function () {
                                SELFASSESSMENT.Utility.SubmitRemoveLoading(btnupdate);
                                SELFASSESSMENT.Utility.ConstructNotificationError(result.message);
                            }, 500);
                        }
                    });
                } else {
                    setTimeout(function () {
                        SELFASSESSMENT.Utility.SubmitRemoveLoading(btnupdate, 'Update');
                    }, 300);
                }
            });
        };

        var inputChangeRemoveErr = function () {
            address.on('input', function () {
                if (this.value.length > 0) {
                    $(this).removeClass('invalid');
                    address.next().remove();
                }
            });

            masterdatakelurahanid.change(function (e) {
                if (this.value !== '' || this.value !== null)
                    $('.err-select-Kelurahan').remove();
            });

            $('input[name="gender"]:radio').change(function () {
                if (this.value !== '' || this.value !== null)
                    $('.errGender').remove();
            });
        };

        var run = function () {
            initialDom();
            pageload();
            getValueRadiogender();
            kelurahanOnchanged();
            genderOnChanged();
            saveEmployee();
            updateEmployee();
            inputChangeRemoveErr();
        };

        return {
            run: run
        };

    })();

    initialEmployee.run();
})(jQuery);