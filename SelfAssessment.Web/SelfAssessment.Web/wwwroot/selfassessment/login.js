﻿(function ($) {
    'use strict';

    var initialLogin = (function () {
        var id, userlevel, userid, lblUserid, password, lblpassword, errorMessage, isRememberMe, btnLogin, loading, eyePass;

        var initialDom = function () {
            userid = $('#UserId');
            lblUserid = $('#lbl_User_Id');
            password = $('#Password');
            lblpassword = $('#lbl_Password');
            errorMessage = $('#btn-message-error');
            isRememberMe = $('#defaultLoginFormRemember');
            btnLogin = $('.btn-login');
            loading = $('#loginLoading');
            eyePass = $('#eye-Pwd');
            checkCookie();
        };

        var formValidation = function () {
            var isFormValid = false,
                isUserId = false,
                isPassword = false;

            if (userid.val() == '') {
                userid.addClass('invalid');
                if ($('.errUserId').length == 0) {
                    userid.after('<div class="invalid-feedback errUserId">' + 'Please fill the required field.' + '</div>');
                    isUserId = false;
                }
            } else {
                userid.removeClass('invalid');
                $('.errUserId').remove();
                isUserId = true;
            }

            if (password.val() == '') {
                $('.errPassword').remove();
                password.addClass('invalid');
                if ($('.errPassword').length == 0) {
                    password.after('<div class="invalid-feedback errPassword">' + 'Please fill the required field.' + '</div>');
                    isPassword = false;
                }
            } else {
                password.removeClass('invalid');
                $('.errPassword').remove();
                isPassword = true;
            }

            if (isUserId && isPassword) {
                isFormValid = true;
            } else {
                isFormValid = false;
            }

            return isFormValid;
        };
       
        var formLoginValidation = function () {
            SELFASSESSMENT.Utility.SubmitLoading(btnLogin);
            if (formValidation()) {

                var users = userid.val().toLowerCase();

                var params = {
                    UserId: SELFASSESSMENT.Utility.EncryptionHandler(users),
                    Password: SELFASSESSMENT.Utility.EncryptionHandler(password.val())
                };

                $.when(SELFASSESSMENT.Services.POSTLocal(params, SELFASSESSMENT.URLContext.Login)).done(function (result, status, xhr) {
                    if (result.code === 200) {
                        if (isRememberMe.is(':checked'))
                            SELFASSESSMENT.CookieConfiguration.SetCookie("userid", userid.val(), 365);

                        id = result.data.id;
                        userlevel = result.data.userLevel;

                        if (userlevel == 2) {
                            window.location.href = SELFASSESSMENT.URLContext.Survey;
                        } else {
                            window.location.href = SELFASSESSMENT.URLContext.Dashboard;
                        }
                    } else {

                        $('.errPassword').remove();
                        password.addClass('invalid');
                        password.after('<div class="invalid-feedback errPassword">' + result.message + '</div>');

                        setTimeout(function () {
                            SELFASSESSMENT.Utility.SubmitRemoveLoading(btnLogin, 'Login');
                            btnLogin.prop('disabled', false);
                        }, 300);
                    }
                });
            } else {
                setTimeout(function () {
                    SELFASSESSMENT.Utility.SubmitRemoveLoading(btnLogin, 'Login');
                    btnLogin.prop('disabled', false);
                    console.log(userid.val());
                }, 300);
            }
        }

        var eventSubmitForm = function () {
            btnLogin.click(
                function (event) {
                    event.preventDefault();
                    formLoginValidation();
                }
            );
        };

        var eventEnterSubmitForm = function () {
            userid.keypress(function (e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    formLoginValidation();
                }
            });

            password.keypress(function (e) {
                if (e.keyCode === 13) {
                    e.preventDefault();
                    formLoginValidation();
                }
            });
        }

        var inputChangeRemoveErr = function () {
            userid.on('input', function (e) {
                $(this).val($(this).val().replace(/[//\\]/gi, ''));

                if (this.value.length > 0) {
                    errorMessage.prop('hidden', true);
                    $(this).removeClass('invalid');
                    userid.next().remove();
                }
            });

            password.on('input', function () {
                if (this.value.length > 0) {
                    errorMessage.prop('hidden', true);
                    $(this).removeClass('invalid');
                    password.next().remove();
                }
            });
        };

        var checkCookie = function () {
            var userIdCookie = SELFASSESSMENT.CookieConfiguration.GetCookie('userid');

            if (userIdCookie != "") {
                userid.val(userIdCookie);
                lblUserid.addClass('active');
                isRememberMe.prop('checked', true);
            }
        };

        var eventShowPassword = function ()
        {
            eyePass.on('mousedown touchstart', function () {
                password.attr("type", "text");
            });

            eyePass.on('mouseup touchend', function () {
                password.attr("type", "password");
            });
        }

        var run = function () {
            initialDom();
            inputChangeRemoveErr();
            eventSubmitForm();
            eventShowPassword();
            eventEnterSubmitForm();
        };

        return {
            run: run
        };
    })();

    initialLogin.run();
})(jQuery);
