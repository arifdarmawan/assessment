﻿(function ($) {
    'use strict';

    var initialComplete = (function () {
        var userid, btnshowResult;

        var initialDom = function () {
            userid = $('#MasterDataUserId');
            btnshowResult = $('.btn-Show-Result');
        };

        var getResult = function () {
            btnshowResult.click(function () {
                SELFASSESSMENT.Utility.SubmitLoading(btnshowResult);
                window.location.href = SELFASSESSMENT.URLContext.Result;
            });
        };

        var run = function () {
            initialDom();
            getResult();
        };

        return {
            run: run
        };
    })();

    initialComplete.run();
})(jQuery);
