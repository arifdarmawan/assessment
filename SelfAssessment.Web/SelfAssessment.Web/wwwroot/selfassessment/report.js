﻿(function ($) {
    'use strict';

    var initialReport = (function () {
        let grid, start_date, end_date, datePicker, modaldetailReport, dataReport;

        var initialDom = function () {
            document.getElementsByClassName("datesearchbox")[0].style.textAlign = "right";
            datePicker = $('#datesearch').daterangepicker({ autoUpdateInput: false });
            modaldetailReport = $('#detail-report');
            dataReport = [];
        };

        var initialGridReportList = function () {
            grid = $('#grid-report').DataTable({
                //data: dataReport,
                //ajax: { url: SELFASSESSMENT.URLContext.GetReportSurvey, dataSrc: '' },
                dom: 'lBfrtip',
                buttons: [
                    {
                        extend: 'excelHtml5',
                        text: 'Export to Excel'
                    }
                ],
                searching: true,
                info: true,
                scrollX: true,
                scrollCollapse: true,
                columns: [
                    { data: "masterDataUserId", title: "Id", visible: false },
                    { data: "tanggal", title: "Tanggal", display: 'mm/dd/yy' },
                    { data: "nik", title: "NIK" },
                    { data: "name", title: "Nama" },
                    { data: "email", title: "Email" },
                    { data: "regionOffice", title: "Region" },
                    { data: "divisi", title: "Divisi" },
                    { data: "gender", title: "Gender" },
                    { data: "address", title: "Alamat" },
                    { data: "kelurahan", title: "Kelurahan" },
                    {
                        data: 'colorCode', title: "Kode Warna",
                        createdCell: function (td, cellData, rowData, row, col) {
                            if (cellData === 'Merah') {
                                $(td).html('<span class="badge badge-danger">' + "MERAH" + '</span>');
                            } else if (cellData === 'Kuning') {
                                $(td).html('<span class="badge badge-warning">' + "KUNING" + '</span>');
                            } else {
                                $(td).html('<span class="badge badge-success">' + "HIJAU" + '</span>');
                            }
                        }
                    },
                    { data: "jamMasuk", title: "Jam Masuk", className: "text-right" },
                    { data: "jamPulang", title: "Jam Keluar", className: "text-right" },
                    { data: "q1", title: "Q1" },
                    { data: "q2", title: "Q2" },
                    { data: "q3", title: "Q3" },
                    { data: "q4", title: "Q4" },
                    { data: "q5", title: "Q5" },
                    { data: "q6", title: "Q6" },
                    { data: "q7", title: "Q7" },
                    { data: "q8", title: "Q8" },
                    { data: "q9", title: "Q9" },
                    { data: "q10", title: "Q10" },
                    { data: "q11", title: "Q11" },
                    { data: "q12", title: "Q12" },
                    { data: "q13", title: "Q13" },
                    { data: "q14", title: "Q14" },
                    { data: "q15", title: "Q15" },
                    { data: "q16", title: "Q16" },
                    { data: "q17", title: "Q17" },
                    { data: "q18", title: "Q18" },
                ],
                columnDefs: [{
                    targets: 1, render: function (data) {
                        return moment(data).format('DD/MM/YYYY');
                    }
                }],
                "rowCallback": function (row, data, index) {
                    if (data.colorCode == 1) {
                        $('td', row).addClass('table-danger');
                    } else if (data.colorCode == 2) {
                        $('td', row).addClass('table-warning');
                    } else if (data.colorCode == 3) {
                        $('td', row).addClass('table-success');
                    }
                },
                "order": [[3, 'asc']],
                select: true,
                initComplete: function (settings, json) {
                    $('#grid-report').find('thead th').addClass('font-weight-bold');
                }
            });
        };

        var gridOnpageload = function () {
            $.when(SELFASSESSMENT.Services.GET(SELFASSESSMENT.URLContext.GetReportSurvey)).done(function (result, status, xhr) {
                setTimeout(function () {
                    //dataReport = result;
                    grid.clear();
                    if (result.data != null) {
                        grid.rows.add(result.data);
                    }
                    grid.draw();
                }, 500);
            });
        }

        var gridOnlick = function () {
            grid.on('click', 'tbody tr', function () {
                var data = grid.row(this).data();
                loadDetailReport(data);
            });
        };

        var loadDetailReport = function (model) {
            $.when(modaldetailReport.find('.modal-body').settingWrapCenter()).done(modaldetailReport.find('.modal-body').componentAddLoading());
            $.when(SELFASSESSMENT.Services.POSTLocal(model, SELFASSESSMENT.URLContext.ReportDetail)).done(function (result, status, xhr) {
                setTimeout(function () {
                    $.when(modaldetailReport.find('.modal-body').resetWrapCenter()).done(modaldetailReport.find('.modal-body').html(result));
                    modaldetailReport.modal('show');
                }, 500);
            });
        };

        var DateFilterFunction = (function (oSettings, aData, iDataIndex) {
            var dateStart = parseDateValue(start_date);
            var dateEnd = parseDateValue(end_date);

            var evalDate = parseDateValue(aData[1]);
            if ((isNaN(dateStart) && isNaN(dateEnd)) ||
                (isNaN(dateStart) && evalDate <= dateEnd) ||
                (dateStart <= evalDate && isNaN(dateEnd)) ||
                (dateStart <= evalDate && evalDate <= dateEnd)) {
                return true;
            }
            return false;
        });

        function parseDateValue(rawDate) {
            var dateArray = rawDate.split("/");
            var parsedDate = new Date(dateArray[2], parseInt(dateArray[1]) - 1, dateArray[0]);
            return parsedDate;
        }

        var initialDateRange = function () {
            datePicker.on('apply.daterangepicker', function (ev, picker) {
                
                $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
                start_date = picker.startDate.format('DD/MM/YYYY');
                end_date = picker.endDate.format('DD/MM/YYYY');

                //$.fn.dataTableExt.afnFiltering.push(DateFilterFunction);
                //grid.draw();

                var model =
                {
                    StartDate: picker.startDate.format('MM/DD/YYYY'),
                    EndDate: picker.endDate.format('MM/DD/YYYY')
                };

                $.when(SELFASSESSMENT.Services.POSTLocal(model, SELFASSESSMENT.URLContext.GetReportSurveyByDateRange)).done(function (result, status, xhr) {
                    setTimeout(function () {
                        grid.clear();
                        if (result.data != null) {
                            grid.rows.add(result.data);
                        }
                        grid.draw();
                    }, 500);
                });
            });

            datePicker.on('cancel.daterangepicker', function (ev, picker) {
                $(this).val('');
                start_date = '';
                end_date = '';
                $.fn.dataTable.ext.search.splice($.fn.dataTable.ext.search.indexOf(DateFilterFunction, 1));
                grid.draw();
            }); 
        };

        var run = function () {
            initialDom();
            initialGridReportList();
            gridOnpageload();
            gridOnlick();
            initialDateRange();
        };

        return {
            run: run
        };
    })();

    initialReport.run();
})(jQuery);
