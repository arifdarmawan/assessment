﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SelfAssessment.Web.Models.ResponseModel;
using SelfAssessment.Web.Models.Survey;
using SelfAssessment.Web.Service;

namespace SelfAssessment.Web.Controllers
{
    public class SurveyController : BaseController
    {
        GlobalService _globalService;
        IConfiguration _configuration;
        IHostingEnvironment _environment;

        public SurveyController(GlobalService globalService, IConfiguration configuration, IHostingEnvironment environment)
        {
            _globalService = globalService;
            _configuration = configuration;
            _environment = environment;          
        }

        [AllowAnonymous, IsAuthenticated]
        [NoDirectAccess]
        [Route("/Survey")]
        [Route("/Survey/Index")]
        public async Task<IActionResult> Index()
        {
            DataSurveyModel model = new DataSurveyModel();

            var result = await _globalService.GetAsync(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:GetPersonalDatabyUserId") + "?userid=" + Id);

            if (result.Data != null)
            {
                model = result.Data != null ? JsonConvert.DeserializeObject<DataSurveyModel>(Convert.ToString(result.Data)) : null;

                if (model.Gender == 0 || model.MasterDataKelurahanId == 0)
                {
                    return RedirectToAction("Add", "User");
                }
                else
                {
                    var resSurvey = await _globalService.GetAsync(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:GetDataSurveybyDateNow") + "?employeeid=" + Id);

                    if (resSurvey.Data != null)
                    {
                        return RedirectToAction("Complete", "Survey");
                    }
                }
            }
            else
            {
                result.Message = "No Result";
            }

            return View(model);
        }

        [HttpPost]
        [Route("Survey/SubmitData")]
        public async Task<BaseResponse> SubmitData(DataSurveyModel values)
        {
            values.CreatedBy = UserName;
            values.CreatedDate = DateTime.Now;
            values.ModifyBy = UserName;
            values.ModifyDate = DateTime.Now;
            values.Region = Region;

            var response = await _globalService.PostAsync(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:InsertDataSurvey"), values);

            return response;
        }

        [AllowAnonymous, IsAuthenticated]
        [NoDirectAccess]
        [Route("/Survey/Result")]
        public async Task<IActionResult> Result()
        {
            var response = await _globalService.GetAsync(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:GetQrCode") + "?userId=" + Id);
            ViewBag.QrCode = "data:image/png;base64," + response.Data;
            ViewBag.ColorCode = response.Code;
            return View();
        }

        [AllowAnonymous, IsAuthenticated]
        [NoDirectAccess]
        [Route("/Survey/Complete")]
        public IActionResult Complete()
        {
            ViewBag.Id = Id;
            return View();
        }
    }
}