﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SelfAssessment.Web.Controllers
{
    public class BaseController : Controller
    {
        const string IdSessionKey = "_Id";
        const string UserIdSessionKey = "_UserId";
        const string UserNameSessionKey = "_UserName";
        const string UserEmailSessionKey = "_UserEmail";
        const string UserLevelSessionKey = "_UserLevel";
        const string RegionSessionKey = "_Region";

        protected string Id
        {
            get { return HttpContext.Session?.GetString(IdSessionKey) ?? ""; }
            set { HttpContext.Session.SetString(IdSessionKey, value); }
        }

        protected string UserId
        {
            get { return HttpContext.Session?.GetString(UserIdSessionKey) ?? ""; }
            set { HttpContext.Session.SetString(UserIdSessionKey, value); }
        }

        protected string UserName
        {
            get { return HttpContext.Session?.GetString(UserNameSessionKey) ?? ""; }
            set { HttpContext.Session.SetString(UserNameSessionKey, value); }
        }

        protected string UserEmail
        {
            get { return HttpContext.Session?.GetString(UserEmailSessionKey) ?? ""; }
            set { HttpContext.Session.SetString(UserEmailSessionKey, value); }
        }

        protected string UserLevel
        {
            get { return HttpContext.Session?.GetString(UserLevelSessionKey) ?? ""; }
            set { HttpContext.Session.SetString(UserLevelSessionKey, value); }
        }

        protected string Region
        {
            get { return HttpContext.Session?.GetString(RegionSessionKey) ?? ""; }
            set { HttpContext.Session.SetString(RegionSessionKey, value); }
        }

        protected void SetCookie(int id, int userLevel)
        {
            UserId = id.ToString();

            switch (userLevel)
            {
                case 1:
                    UserLevel = "SUPERADMIN";
                    break;
                case 2:
                    UserLevel = "USER";
                    break;
                case 3:
                    UserLevel = "ADMIN";
                    break;
                default:
                    break;
            }
        }

        protected void SetCookieId(int id)
        {
            Id = id.ToString();
        }

        protected void SetCookieUserId(string userId)
        {
            UserId = userId;
        }

        protected void SetCookieUserName(string name)
        {
            UserName = name;
        }

        protected void SetCookieUserEmail(string email)
        {
            UserEmail = email;
        }

        protected void SetCookieRegion(string region)
        {
            Region = region;
        }
    }
}
