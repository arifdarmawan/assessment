﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SelfAssessment.Web.Models.Admin;
using SelfAssessment.Web.Models.Region;
using SelfAssessment.Web.Models.ResponseModel;
using SelfAssessment.Web.Service;

namespace SelfAssessment.Web.Controllers
{
    public class AdminController : BaseController
    {
        GlobalService _globalService;
        IConfiguration _configuration;
        IHostingEnvironment _environment;

        public AdminController(GlobalService globalService, IConfiguration configuration, IHostingEnvironment environment)
        {
            _globalService = globalService;
            _configuration = configuration;
            _environment = environment;
        }

        [AllowAnonymous, IsAuthenticated]
        [NoDirectAccess]
        [Route("/Admin")]
        [Route("/Admin/Dashboard")]
        public async Task<IActionResult> Dashboard()
        {
            DashboardModel model = new DashboardModel();

            var result = await _globalService.GetAsync(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:getTotalResultUserbyColorCode") + "?userlevel=" + UserLevel + "&region=" + Region);

            if (result.Data != null)
            {
                model = result.Data != null ? JsonConvert.DeserializeObject<DashboardModel>(Convert.ToString(result.Data)) : null;
            }
            else
            {
                result.Message = "No Result";
            }
            return View(model);
        }

        [AllowAnonymous, IsAuthenticated]
        [NoDirectAccess]
        [Route("/Admin/Report")]
        public IActionResult Report()
        {
            return View();
        }

        [Route("/Admin/GetReportSurvey")]
        public async Task<BaseResponse> GetReportSurvey()
        {
            BaseResponse response = new BaseResponse();

            var result = await _globalService.GetAsync(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:GetResultUSerSurveybyDateNow") + "?userlevel=" + UserLevel + "&region=" + Region);
            List<ResultUserModel> model = result.Data != null ? JsonConvert.DeserializeObject<List<ResultUserModel>>(Convert.ToString(result.Data)) : null;

            if (result != null)
            {
                response.Data = model;
            }
            else
            {
                response.Message = result.Message;
                response.Code = result.Code;
            }

            return response;
        }

        [Route("/Admin/GetReportSurveyByDateRange")]
        public async Task<BaseResponse> GetReportSurveyByDateRange(DateRange data)
        {
            BaseResponse response = new BaseResponse();

            var result = await _globalService.GetAsync(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:GetResultSurveyByStartAndEndDate") + "?startDate=" + data.StartDate + "&endDate=" + data.EndDate + "&userlevel=" + UserLevel + "&region=" + Region);
            List<ResultUserModel> model = result.Data != null ? JsonConvert.DeserializeObject<List<ResultUserModel>>(Convert.ToString(result.Data)) : null;

            if (result != null)
            {
                response.Data = model;
            }
            else
            {
                response.Message = result.Message;
                response.Code = result.Code;
            }

            return response;
        }

        [Route("/Admin/ReportDetail")]
        public async Task<IActionResult> ReportDetail(ResultUserModel data)
        {
            var result = await _globalService.GetAsync(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:GetDetailResultSurvey") + "?userId=" + data.MasterDataUserId + "&date=" + data.Tanggal);
            List<ResultSurveyModel> model = result.Data != null ? JsonConvert.DeserializeObject<List<ResultSurveyModel>>(Convert.ToString(result.Data)) : null;

            return PartialView(model);
        }

        [AllowAnonymous, IsAuthenticated]
        [NoDirectAccess]
        [Route("/Admin/Users")]
        public async Task<IActionResult> Users()
        {
            UserAdminModel model = new UserAdminModel();

            var resRegion = await _globalService.GetAsync(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:GetDataRegion"));

            List<RegionModel> region = resRegion.Data != null ? JsonConvert.DeserializeObject<List<RegionModel>>(Convert.ToString(resRegion.Data)) : null;

            model.RegionOptions = model.ConstructRegionOptions(region);

            return View(model);
        }

        [HttpGet]
        [Route("/Admin/GetUserAdmin")]
        public async Task<string> GetUserAdmin()
        {
            var result = await _globalService.GetAsync(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:GetDataUserAdmin"));

            return JsonConvert.SerializeObject(result.Data);
        }

        [HttpPost]
        [Route("Admin/SubmitUser")]
        public async Task<BaseResponse> SubmitData(UserAdminModel values)
        {
            values.CreatedBy = UserName;
            values.CreatedDate = DateTime.Now;
            values.ModifyBy = UserName;
            values.ModifyDate = DateTime.Now;

            var response = await _globalService.PostAsync(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:InsertUserAdmin"), values);

            return response;
        }

        [HttpPost]
        [Route("Admin/UpdateUser")]
        public async Task<BaseResponse> UpdateUser(UserAdminModel values)
        {
            values.CreatedBy = UserName;
            values.CreatedDate = DateTime.Now;
            values.ModifyBy = UserName;
            values.ModifyDate = DateTime.Now;

            var response = await _globalService.PostAsync(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:UpdateUserAdmin"), values);

            return response;
        }

        [HttpGet]
        [Route("Admin/DeleteUser/{id}")]
        public async Task<BaseResponse> DeleteUser(int id)
        {
            var result = await _globalService.GetAsync(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:DeleteUserAdmin") + "?id=" + id);

            return result;
        }

        [AllowAnonymous, IsAuthenticated]
        [NoDirectAccess]
        [Route("/Admin/ChangePassword")]
        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        [Route("Admin/SubmitChangePassword")]
        public async Task<BaseResponse> SubmitChangePassword(ChangePasswordModel values)
        {
            values.UserId = UserId;
            values.ModifyBy = UserName;
            values.ModifyDate = DateTime.Now;

            var response = await _globalService.PostAsync(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:SetNewPassword"), values);

            return response;
        }
    }
}