﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SelfAssessment.Web.Helper;
using SelfAssessment.Web.Models.Account;
using SelfAssessment.Web.Models.ResponseModel;
using SelfAssessment.Web.Service;

namespace SelfAssessment.Web.Controllers
{
    [Route("[controller]/[action]")]
    public class AccountController : BaseController
    {
        readonly GlobalService _globalService;
        readonly IConfiguration _configuration;
        readonly IHostingEnvironment _environment;

        public AccountController(GlobalService globalService,
            IConfiguration configuration,
            IHostingEnvironment environment) {
            _globalService = globalService;
            _configuration = configuration;
            _environment = environment;
        }

        [Route("/")]
        [Route("/[controller]")]
        public IActionResult Index()
        {
            return View();
        }

        #region
        [HttpPost]
        [Route("/Account/Login")]
        public async Task<BaseResponse> Login(LoginViewModel model)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                EncryptionHelper.AESEncryptionHelper(model);

                var resUser = await _globalService.GetAsync(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:GetUserByEmailOrUserId") + "/?userid=" + model.UserId);
                var userModel = resUser.Data != null ? JsonConvert.DeserializeObject<UserModel>(Convert.ToString(resUser.Data)) : null;

                if (userModel == null || userModel.UserLevel == 2)
                {
                    if (resUser.Code == (int)HttpStatusCode.OK)
                    {
                        if (userModel == null)
                        {
                            #region INSERT NEW USER
                            var resUserAD = await _globalService.PostAsync(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:InsertUserFromAD"), model);

                            if (resUserAD.Code == (int)HttpStatusCode.OK)
                            {
                                userModel = resUserAD.Data != null ? JsonConvert.DeserializeObject<UserModel>(Convert.ToString(resUserAD.Data)) : null;

                                response = resUserAD;
                                response.SessionUserLevel = UserLevel;
                            }
                            else if (resUserAD.Code == (int)CustomHttpStatusCode.UserAdNotRegistered)
                            {
                                response.Code = resUserAD.Code;
                                response.Message = resUserAD.Message;
                            }
                            else
                            {
                                response.Code = resUserAD.Code;
                                response.Status = resUserAD.Status;
                                response.Message = resUserAD.Message;
                            }
                            #endregion
                        }
                        else
                        {
                            #region UPDATE DATA USER
                            var resUserAD = await _globalService.PostAsync(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:UpdateUserFromAD"), model);

                            if (resUserAD.Code == (int)HttpStatusCode.OK)
                            {
                                userModel = resUserAD.Data != null ? JsonConvert.DeserializeObject<UserModel>(Convert.ToString(resUserAD.Data)) : null;

                                response = resUserAD;
                                response.SessionUserLevel = UserLevel;
                            }
                            else if (resUserAD.Code == (int)HttpStatusCode.BadRequest)
                            {
                                response.Code = resUserAD.Code;
                                response.Status = resUserAD.Status;
                                response.Message = resUserAD.Message;
                            }
                            else
                            {
                                response.Code = resUserAD.Code;
                                response.Status = resUserAD.Status;
                                response.Message = resUserAD.Message;
                            }
                            #endregion
                        }

                        #region Set Cookie
                        //Logic check data employee before login
                        if (response.Code == (int)HttpStatusCode.OK)
                        {
                            EmployeeModel emp = new EmployeeModel();

                            var checkData = await _globalService.GetAsync(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:GetUserByEmail") + "/?email=" + userModel.UserEmail);
                            emp = checkData.Data != null ? JsonConvert.DeserializeObject<EmployeeModel>(Convert.ToString(checkData.Data)) : null;

                            if (checkData.Code == (int)HttpStatusCode.OK)
                            {
                                // Set Cookie for Profile Global Setting
                                SetCookie(userModel.Id, userModel.UserLevel);
                                SetCookieId(userModel.Id);
                                SetCookieUserId(model.UserId);
                                SetCookieUserName(emp.EmployeeName);
                                SetCookieUserEmail(emp.Email);
                                SetCookieRegion(emp.RegionOffice);
                            }
                            else if (checkData.Code == (int)CustomHttpStatusCode.EmployeeDataNotExist)
                            {
                                response.Code = checkData.Code;
                                response.Message = "Employee data is not exist. Please contact administrator.";
                            }
                            else
                            {
                                response.Code = checkData.Code;
                                response.Status = checkData.Status;
                                response.Message = "Internal Server Error. Please Contact Administrator.";
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        response.Code = resUser.Code;
                        response.Status = resUser.Status;
                        response.Message = "Internat Server Error. Please Contact Administrator.";
                    }
                }
                else {                    
                    if (EncryptionExtensions.DecryptStringAES(userModel.Password) != model.Password)
                    {
                        response.Code = 101;
                        response.Status = HttpStatusCode.BadRequest.ToString();
                        response.Message = "Username and password not match.";
                    }
                    else 
                    {
                        response = resUser;

                        EmployeeModel emp = new EmployeeModel();

                        var checkData = await _globalService.GetAsync(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:GetUserByEmail") + "/?email=" + userModel.UserEmail);
                        emp = checkData.Data != null ? JsonConvert.DeserializeObject<EmployeeModel>(Convert.ToString(checkData.Data)) : null;


                        if (checkData.Code == (int)HttpStatusCode.OK) 
                        {
                            // Set Cookie for Profile Global Setting
                            SetCookie(userModel.Id, userModel.UserLevel);
                            SetCookieId(userModel.Id);
                            SetCookieUserId(model.UserId);
                            SetCookieUserName(emp.EmployeeName);
                            SetCookieRegion(emp.RegionOffice);
                        }
                    }
                }
            }
            catch
            {
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Message = "Internat Server Error. Please Contact Administrator.";
            }

            return response;
        }

        [Route("/Account/Logout")]
        public IActionResult Logout()
        {
            // Clear all session
            HttpContext.Session.Clear();

            //return View();
            return RedirectToAction("Index", "Account");
        }
        #endregion
    }
}