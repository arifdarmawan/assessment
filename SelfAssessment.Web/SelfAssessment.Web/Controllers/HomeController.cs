﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;


namespace SelfAssessment.Web.Controllers
{
    public class HomeController : BaseController
    {
        IConfiguration _configuration;
        readonly string _baseHRISServerKey;
        public HomeController(IConfiguration configuration)
        {
            _configuration = configuration;
            _baseHRISServerKey = _configuration.GetValue<string>("BaseServer") + "HRISAPIEndpoint";
        }

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult Dashboard()
        {
            ViewData["UserLevel"] = UserLevel;

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Faq() 
        {
            return View();
        }

        public async Task<string> GetUrlApiSelfAssessment()
        {
            return await Task.FromResult(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:BaseUrl"));
        }

        public async Task<string> GetUrlApiHRIS()
        {
            return await Task.FromResult(_configuration.GetValue<string>(_baseHRISServerKey + ":BaseUrl"));
        }

        [Route("/Home/Error/{code:int}")]
        public IActionResult Error()
        {
            //// Clear all session
            //HttpContext.Session.Clear();

            return View("~/Views/Shared/Error.cshtml");
        }

        public void UpdateSession()
        {

        }
    }
}
