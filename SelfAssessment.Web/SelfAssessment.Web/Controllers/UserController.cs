﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using SelfAssessment.Web.Models.Account;
using SelfAssessment.Web.Models.Kelurahan;
using SelfAssessment.Web.Models.ResponseModel;
using SelfAssessment.Web.Service;

namespace SelfAssessment.Web.Controllers
{
    public class UserController : BaseController
    {
        GlobalService _globalService;
        IConfiguration _configuration;
        IHostingEnvironment _environment;

        public UserController(GlobalService globalService, IConfiguration configuration, IHostingEnvironment environment)
        {
            _globalService = globalService;
            _configuration = configuration;
            _environment = environment;
        }

        [AllowAnonymous, IsAuthenticated]
        [NoDirectAccess]
        [Route("/User/Add")]
        public async Task<IActionResult> Add()
        {
            EmployeeModel model = new EmployeeModel();

            var result = await _globalService.GetAsync(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:GetDataUserAD") + "?userid=" + Id);

            if (result.Data != null)
            {
                model = result.Data != null ? JsonConvert.DeserializeObject<EmployeeModel>(Convert.ToString(result.Data)) : null;

                var reskelurahan = await _globalService.GetAsync(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:GetDataKelurahanByRegionCode") + "?region=" + Region);

                if (reskelurahan.Data != null)
                {
                    List<KelurahanModel> kelurahan = reskelurahan.Data != null ? JsonConvert.DeserializeObject<List<KelurahanModel>>(Convert.ToString(reskelurahan.Data)) : null;
                    model.KelurahanOptions = model.ConstructKelurahanOptions(kelurahan);
                }
            }

            return View(model);
        }

        [AllowAnonymous, IsAuthenticated]
        [NoDirectAccess]
        [Route("/User/Update")]
        public async Task<IActionResult> Update()
        {
            EmployeeModel model = new EmployeeModel();

            var result = await _globalService.GetAsync(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:GetEmployeeByUserId") + "?userid=" + Id);

            if (result.Data != null)
            {
                model = result.Data != null ? JsonConvert.DeserializeObject<EmployeeModel>(Convert.ToString(result.Data)) : null;
            }
            else
            {
                var result2 = await _globalService.GetAsync(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:GetDataUserAD") + "?userid=" + Id);

                model = result2.Data != null ? JsonConvert.DeserializeObject<EmployeeModel>(Convert.ToString(result2.Data)) : null;
            }

            var resKelurahan = await _globalService.GetAsync(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:GetDataKelurahanByRegionCode") + "?region=" + Region);

            List<KelurahanModel> kelurahan = resKelurahan.Data != null ? JsonConvert.DeserializeObject<List<KelurahanModel>>(Convert.ToString(resKelurahan.Data)) : null;

            model.KelurahanOptions = model.ConstructKelurahanOptions(kelurahan);

            return View(model);
        }

        [HttpGet]
        [Route("User/GetDataKelurahanById/{masterdatakelurahanid}")]
        public async Task<BaseResponse> GetDataKelurahanById(int masterdatakelurahanid)
        {
            var response = await _globalService.GetAsync(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:GetDataKelurahanById") + "?id=" + masterdatakelurahanid);

            return response;
        }

        [HttpGet]
        public async Task<JsonResult> GetDataKelurahanByCity(int masterdatakotaid)
        {
            var response = await _globalService.GetAsync(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:GetDataKelurahanByCityId") + "/?id=" + masterdatakotaid);

            return Json(response.Data);
        }

        [Route("User/UpdateEmployee")]
        [HttpPost]
        public async Task<BaseResponse> UpdateDataEmployee(EmployeeModel data)
        {
            BaseResponse response = new BaseResponse();

            data.ModifyBy = UserName;
            data.ModifyDate = DateTime.Now;

            var result = await _globalService.PostAsync(_configuration.GetValue<string>("SelfAssessmentAPIEndpoint:UpdateEmployee"), data);

            if (result.Code == (int)HttpStatusCode.OK)
            {
                response = result;
            }
            else
            {
                response.Code = result.Code;
                response.Status = result.Status;
                response.Message = result.Message;
            }

            return response;
        }
    }
}