﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SelfAssessment.Model.Models.User
{
    public class LoginViewModel
    {
        public string UserId { get; set; }
        public string Password { get; set; }
    }
}
