﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SelfAssessment.Model.Models.User
{
    public class NewPasswordModel
    {
        public string Token { get; set; }
        public string NewPassword { get; set; }
    }
}
