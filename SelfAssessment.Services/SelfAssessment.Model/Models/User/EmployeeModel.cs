﻿using System;

namespace SelfAssessment.Model.Models.User
{
    public class EmployeeModel
    {
        public int Id { get; set; }
        public int MasterDataUserId { get; set; }
        public string NIK { get; set; }
        public string EmployeeName { get; set; }
        public int? Gender { get; set; }
        public string Address { get; set; }
        public int? MasterDataKelurahanId { get; set; }
        public string kota { get; set; }
        public string Kecamatan { get; set; }
        public string KodePos { get; set; }
        public string Email { get; set; }
        public string RegionOffice { get; set; }
        public string Jabatan { get; set; }
        public string Divisi { get; set; }
        public string ExtensionNumber { get; set; }
        public bool IsActive { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}