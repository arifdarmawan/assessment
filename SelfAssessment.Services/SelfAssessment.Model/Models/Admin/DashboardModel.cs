﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SelfAssessment.Model.Models.Admin
{
    public class DashboardModel
    {
        public int TotalWarnaMerah { get; set; }
        public int TotalWarnaKuning { get; set; }
        public int TotalWarnaHijau { get; set; }
    }
}
