﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SelfAssessment.Model.Models.Admin
{
    public class ResultSurveyModel
    {
        public string Question { get; set; }
        public string Result { get; set; }
    }
}
