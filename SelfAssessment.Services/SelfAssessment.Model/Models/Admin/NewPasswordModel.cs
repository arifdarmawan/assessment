﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SelfAssessment.Model.Models.Admin
{
    public class NewPasswordModel
    {
        public string UserId { get; set; }
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
        public string ConfirmPassword { get; set; }
        public string ModifyBy { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}
