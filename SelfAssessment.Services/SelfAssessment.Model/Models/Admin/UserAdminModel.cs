﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SelfAssessment.Model.Models.Admin
{
    public class UserAdminModel
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public int UserRole { get; set; }
        public string RegionId { get; set; }
        public string Region { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}
