﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SelfAssessment.Model.Models.Reader
{
    public class ReaderModel
    {
        public string QRCode { get; set; }
        public string Time { get; set; }
    }
}
