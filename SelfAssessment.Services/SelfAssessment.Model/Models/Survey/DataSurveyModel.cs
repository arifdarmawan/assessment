﻿using System;

namespace SelfAssessment.Model.Models.Survey
{
    public class DataSurveyModel
    {
        // Personal Data
        public int MasterDataUserId { get; set; }
        public string NIK { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Region { get; set; }
        public int Gender { get; set; }
        public int? MasterDataKelurahanId { get; set; }
        public string Address { get; set; }
        public string PlaceStateStatus { get; set; }
        public string LivingWith { get; set; }
        public string Question1 { get; set; }
        public string Question2 { get; set; }
        public string Question3 { get; set; }
        public string Question4 { get; set; }
        public string Question5 { get; set; }
        public string Question6 { get; set; }
        public string Question7 { get; set; }
        public string Question8 { get; set; }
        public string Question9 { get; set; }
        public string Question10 { get; set; }
        public string Question11 { get; set; }
        public string Question12 { get; set; }
        public string Question13 { get; set; }
        public string Question14 { get; set; }
        public string Question15 { get; set; }
        public string Question16 { get; set; }
        public string Question17 { get; set; }
        public string Question18 { get; set; }
        public bool IsApproved { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}
