﻿using System;
using System.Collections.Generic;

namespace SelfAssessment.Model.Entities
{
    public partial class MasterDataResult
    {
        public int Id { get; set; }
        public int MasterDataUserId { get; set; }
        public string Nama { get; set; }
        public int ColorCode { get; set; }
        public string Images { get; set; }
        public string Qrcode { get; set; }
        public string JamMasuk { get; set; }
        public string JamPulang { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime ModifyDate { get; set; }
    }
}
