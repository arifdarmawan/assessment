﻿using System;
using System.Collections.Generic;

namespace SelfAssessment.Model.Entities
{
    public partial class MasterDataPlace
    {
        public int Id { get; set; }
        public string Place { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime? ModifyDate { get; set; }
    }
}
