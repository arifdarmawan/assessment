﻿using System;
using System.Collections.Generic;

namespace SelfAssessment.Model.Entities
{
    public partial class MasterDataMappingSurveyQuestion
    {
        public int Id { get; set; }
        public int MasterDataUserId { get; set; }
        public string Q1 { get; set; }
        public string Qcode1 { get; set; }
        public string Q2 { get; set; }
        public string Qcode2 { get; set; }
        public string Q3 { get; set; }
        public string Qcode3 { get; set; }
        public string Q5 { get; set; }
        public string Qcode5 { get; set; }
        public string Q6 { get; set; }
        public string Qcode6 { get; set; }
        public string Q7 { get; set; }
        public string Qcode7 { get; set; }
        public string Q8 { get; set; }
        public string Qcode8 { get; set; }
        public string Q9 { get; set; }
        public string Qcode9 { get; set; }
        public string Q10 { get; set; }
        public string Qcode10 { get; set; }
        public string Q11 { get; set; }
        public string Qcode11 { get; set; }
        public string Q12 { get; set; }
        public string Qcode12 { get; set; }
        public string Q13 { get; set; }
        public string Qcode13 { get; set; }
        public string Q14 { get; set; }
        public string Qcode14 { get; set; }
        public string Q15 { get; set; }
        public string Qcode15 { get; set; }
        public string Q16 { get; set; }
        public string Qcode16 { get; set; }
        public string Q17 { get; set; }
        public string Qcode17 { get; set; }
        public string Q18 { get; set; }
        public string Qcode18 { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime ModifyDate { get; set; }
        public bool IsDeleted { get; set; }
    }
}
