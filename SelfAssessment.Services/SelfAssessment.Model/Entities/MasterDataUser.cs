﻿using System;
using System.Collections.Generic;

namespace SelfAssessment.Model.Entities
{
    public partial class MasterDataUser
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public string ForgetPassword { get; set; }
        public string UserEmail { get; set; }
        public int UserLevel { get; set; }
        public string Image { get; set; }
        public string Session { get; set; }
        public DateTime? LastLogin { get; set; }
        public DateTime? LastLogout { get; set; }
        public string Domain { get; set; }
        public string ActivateLink { get; set; }
        public string RegistrationNumber { get; set; }
        public bool IsActive { get; set; }
        public bool? IsDelete { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string ModifyBy { get; set; }
        public DateTime? ModifyDate { get; set; }
    }
}
