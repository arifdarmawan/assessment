﻿using System;
using System.Collections.Generic;

namespace SelfAssessment.Model.Entities
{
    public partial class MasterDataDomain
    {
        public string DomainName { get; set; }
        public string DomainLink { get; set; }
        public bool? IsDelete { get; set; }
    }
}
