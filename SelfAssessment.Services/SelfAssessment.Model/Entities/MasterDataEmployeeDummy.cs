﻿using System;
using System.Collections.Generic;

namespace SelfAssessment.Model.Entities
{
    public partial class MasterDataEmployeeDummy
    {
        public int Id { get; set; }
        public string Nik { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
