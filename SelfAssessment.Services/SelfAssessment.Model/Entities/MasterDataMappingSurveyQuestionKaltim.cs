﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SelfAssessment.Model.Entities
{
    public partial class MasterDataMappingSurveyQuestionKaltim
    {
        [Key]
        public int Id { get; set; }
        public int MasterDataUserId { get; set; }
        [Required]
        public string Q1 { get; set; }
        [Column("QCode1")]
        [StringLength(2)]
        public string Qcode1 { get; set; }
        [Required]
        public string Q2 { get; set; }
        [Column("QCode2")]
        [StringLength(2)]
        public string Qcode2 { get; set; }
        [Required]
        public string Q3 { get; set; }
        [Column("QCode3")]
        [StringLength(2)]
        public string Qcode3 { get; set; }
        [Required]
        public string Q5 { get; set; }
        [Column("QCode5")]
        [StringLength(2)]
        public string Qcode5 { get; set; }
        [Required]
        public string Q6 { get; set; }
        [Column("QCode6")]
        [StringLength(2)]
        public string Qcode6 { get; set; }
        [Required]
        public string Q7 { get; set; }
        [Column("QCode7")]
        [StringLength(2)]
        public string Qcode7 { get; set; }
        [Required]
        public string Q8 { get; set; }
        [Column("QCode8")]
        [StringLength(2)]
        public string Qcode8 { get; set; }
        [Required]
        public string Q9 { get; set; }
        [Column("QCode9")]
        [StringLength(2)]
        public string Qcode9 { get; set; }
        [Required]
        public string Q10 { get; set; }
        [Column("QCode10")]
        [StringLength(2)]
        public string Qcode10 { get; set; }
        [Required]
        public string Q11 { get; set; }
        [Column("QCode11")]
        [StringLength(2)]
        public string Qcode11 { get; set; }
        [Required]
        public string Q12 { get; set; }
        [Column("QCode12")]
        [StringLength(2)]
        public string Qcode12 { get; set; }
        [Required]
        public string Q13 { get; set; }
        [Column("QCode13")]
        [StringLength(2)]
        public string Qcode13 { get; set; }
        [Required]
        public string Q14 { get; set; }
        [Column("QCode14")]
        [StringLength(2)]
        public string Qcode14 { get; set; }
        [Required]
        public string Q15 { get; set; }
        [Column("QCode15")]
        [StringLength(2)]
        public string Qcode15 { get; set; }
        [Required]
        public string Q16 { get; set; }
        [Column("QCode16")]
        [StringLength(2)]
        public string Qcode16 { get; set; }
        [Required]
        public string Q17 { get; set; }
        [Column("QCode17")]
        [StringLength(2)]
        public string Qcode17 { get; set; }
        [Required]
        public string Q18 { get; set; }
        [Column("QCode18")]
        [StringLength(2)]
        public string Qcode18 { get; set; }
        [StringLength(50)]
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        [Required]
        [StringLength(50)]
        public string ModifyBy { get; set; }
        public DateTime ModifyDate { get; set; }
        public bool IsDeleted { get; set; }
    }
}
