﻿using System;
using System.Collections.Generic;

namespace SelfAssessment.Model.Entities
{
    public partial class MasterDataKelurahan
    {
        public int Id { get; set; }
        public string Kelurahan { get; set; }
        public string Kecamatan { get; set; }
        public string KotaKabupaten { get; set; }
        public string Provinsi { get; set; }
        public int? Kodepos { get; set; }
        public string RegionCode { get; set; }
    }
}
