﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SelfAssessment.Model.Entities
{
    public partial class SelfAssessmentContext : DbContext
    {
        public SelfAssessmentContext()
        {
        }

        public SelfAssessmentContext(DbContextOptions<SelfAssessmentContext> options)
            : base(options)
        {
        }

        public virtual DbSet<MasterDataDomain> MasterDataDomain { get; set; }
        public virtual DbSet<MasterDataEmployee> MasterDataEmployee { get; set; }
        public virtual DbSet<MasterDataEmployeeDummy> MasterDataEmployeeDummy { get; set; }
        public virtual DbSet<MasterDataKelurahan> MasterDataKelurahan { get; set; }
        public virtual DbSet<MasterDataMappingSurveyQuestion> MasterDataMappingSurveyQuestion { get; set; }
        public virtual DbSet<MasterDataPlace> MasterDataPlace { get; set; }
        public virtual DbSet<MasterDataQuestion> MasterDataQuestion { get; set; }
        public virtual DbSet<MasterDataRegion> MasterDataRegion { get; set; }
        public virtual DbSet<MasterDataResult> MasterDataResult { get; set; }
        public virtual DbSet<MasterDataSurvey> MasterDataSurvey { get; set; }
        public virtual DbSet<MasterDataUser> MasterDataUser { get; set; }
        public virtual DbSet<MasterDataUserRole> MasterDataUserRole { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseNpgsql("Host=10.100.1.12;Database=SelfAssessment;Username=postgres;Password=FR*#q1q2q3q4");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<MasterDataDomain>(entity =>
            {
                entity.HasKey(e => e.DomainLink)
                    .HasName("MasterDataDomain_pkey");

                entity.Property(e => e.DomainLink).HasMaxLength(100);

                entity.Property(e => e.DomainName).HasMaxLength(50);
            });

            modelBuilder.Entity<MasterDataEmployee>(entity =>
            {
                entity.Property(e => e.Address).HasMaxLength(225);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Divisi).HasMaxLength(100);

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.EmployeeName)
                    .IsRequired()
                    .HasMaxLength(100);

                entity.Property(e => e.ExtensionNumber).HasMaxLength(20);

                entity.Property(e => e.Jabatan).HasMaxLength(100);

                entity.Property(e => e.ModifyBy)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Nik)
                    .HasColumnName("NIK")
                    .HasMaxLength(50);

                entity.Property(e => e.RegionOffice).HasMaxLength(50);
            });

            modelBuilder.Entity<MasterDataEmployeeDummy>(entity =>
            {
                entity.Property(e => e.Email).HasMaxLength(100);

                entity.Property(e => e.Name).HasMaxLength(100);

                entity.Property(e => e.Nik)
                    .HasColumnName("NIK")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<MasterDataKelurahan>(entity =>
            {
                entity.Property(e => e.Kecamatan).HasMaxLength(50);

                entity.Property(e => e.Kelurahan).HasMaxLength(50);

                entity.Property(e => e.KotaKabupaten)
                    .HasColumnName("Kota_kabupaten")
                    .HasMaxLength(50);

                entity.Property(e => e.Provinsi).HasMaxLength(50);

                entity.Property(e => e.RegionCode).HasMaxLength(50);
            });

            modelBuilder.Entity<MasterDataMappingSurveyQuestion>(entity =>
            {
                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.ModifyBy)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Q1)
                    .IsRequired()
                    .HasDefaultValueSql("'Apakah Anda saat ini dalam kondisi hamil?'::character varying");

                entity.Property(e => e.Q10)
                    .IsRequired()
                    .HasDefaultValueSql("'Apakah anda memiliki history penyakit yang cukup serius seperti kardiovaskular (jantung), gangguan pernafasan kronis (paru-paru), diabetes, ginjal, kanker, hipertensi?'::character varying");

                entity.Property(e => e.Q11)
                    .IsRequired()
                    .HasDefaultValueSql("'Dalam 14 hari terakhir, Apakah Anda pernah kontak dengan Pasien Positif Covid19?'::character varying");

                entity.Property(e => e.Q12)
                    .IsRequired()
                    .HasDefaultValueSql("'Dalam 30 hari terakhir, apakah ada anggota keluarga dalam satu rumah ada dinyatakan positif Covid19?'::character varying");

                entity.Property(e => e.Q13)
                    .IsRequired()
                    .HasDefaultValueSql("'Anggota keluarga dalam satu rumah bekerja di Rumah Sakit/ Klinik, Tempat Pelayanan Umum (restoran, stasiun, bandara, mall, bank atau berjualan di pasar)'::character varying");

                entity.Property(e => e.Q14)
                    .IsRequired()
                    .HasDefaultValueSql("'Apakah ada ditemukan positif Covid19 di lingkungan/ RT tempat tinggal Anda?'::character varying");

                entity.Property(e => e.Q15)
                    .IsRequired()
                    .HasDefaultValueSql("'Apakah anda dalam 30 hari terakhir, melakukan perjalanan dari Luar Jabodetabek?'::character varying");

                entity.Property(e => e.Q16)
                    .IsRequired()
                    .HasDefaultValueSql("'Apakah Anda sudah selesai menjalani isolasi mandiri 14 hari?'::character varying");

                entity.Property(e => e.Q17)
                    .IsRequired()
                    .HasDefaultValueSql("'Apakah Anda sudah melakukan Rapid Test/PCR/Swab Test?'::character varying");

                entity.Property(e => e.Q18)
                    .IsRequired()
                    .HasDefaultValueSql("'Hasil Rapid Test/PCR/Swab Test?'::character varying");

                entity.Property(e => e.Q2)
                    .IsRequired()
                    .HasDefaultValueSql("'Usia'::character varying");

                entity.Property(e => e.Q3)
                    .IsRequired()
                    .HasDefaultValueSql("'Status tempat tinggal'::character varying");

                entity.Property(e => e.Q5)
                    .IsRequired()
                    .HasDefaultValueSql("'Kendaraan yang digunakan ke kantor'::character varying");

                entity.Property(e => e.Q6)
                    .IsRequired()
                    .HasDefaultValueSql("'Dalam 14 hari terakhir, apakah Anda menggunakan transportasi umum termasuk Grab/Gojek/Gocar?'::character varying");

                entity.Property(e => e.Q7)
                    .IsRequired()
                    .HasDefaultValueSql("'Dalam 14 hari terakhir, apakah Anda menghadiri acara dengan peserta lebih dari 10 orang?'::character varying");

                entity.Property(e => e.Q8)
                    .IsRequired()
                    .HasDefaultValueSql("'Dalam 14 hari terakhir, apakah Anda mengunjungi tempat seperti pasar, mall, rumah ibadah, rumah sakit, minimarket diluar komplek seperti indomaret)?'::character varying");

                entity.Property(e => e.Q9)
                    .IsRequired()
                    .HasDefaultValueSql("'Dalam 14 hari terakhir, apakah Anda mengalami sakit dengan gejala batuk/ pilek/ flu/ demam/sakit tenggorokan?'::character varying");

                entity.Property(e => e.Qcode1)
                    .HasColumnName("QCode1")
                    .HasMaxLength(2);

                entity.Property(e => e.Qcode10)
                    .HasColumnName("QCode10")
                    .HasMaxLength(2);

                entity.Property(e => e.Qcode11)
                    .HasColumnName("QCode11")
                    .HasMaxLength(2);

                entity.Property(e => e.Qcode12)
                    .HasColumnName("QCode12")
                    .HasMaxLength(2);

                entity.Property(e => e.Qcode13)
                    .HasColumnName("QCode13")
                    .HasMaxLength(2);

                entity.Property(e => e.Qcode14)
                    .HasColumnName("QCode14")
                    .HasMaxLength(2);

                entity.Property(e => e.Qcode15)
                    .HasColumnName("QCode15")
                    .HasMaxLength(2);

                entity.Property(e => e.Qcode16)
                    .HasColumnName("QCode16")
                    .HasMaxLength(2);

                entity.Property(e => e.Qcode17)
                    .HasColumnName("QCode17")
                    .HasMaxLength(2);

                entity.Property(e => e.Qcode18)
                    .HasColumnName("QCode18")
                    .HasMaxLength(2);

                entity.Property(e => e.Qcode2)
                    .HasColumnName("QCode2")
                    .HasMaxLength(2);

                entity.Property(e => e.Qcode3)
                    .HasColumnName("QCode3")
                    .HasMaxLength(2);

                entity.Property(e => e.Qcode5)
                    .HasColumnName("QCode5")
                    .HasMaxLength(2);

                entity.Property(e => e.Qcode6)
                    .HasColumnName("QCode6")
                    .HasMaxLength(2);

                entity.Property(e => e.Qcode7)
                    .HasColumnName("QCode7")
                    .HasMaxLength(2);

                entity.Property(e => e.Qcode8)
                    .HasColumnName("QCode8")
                    .HasMaxLength(2);

                entity.Property(e => e.Qcode9)
                    .HasColumnName("QCode9")
                    .HasMaxLength(2);
            });

            modelBuilder.Entity<MasterDataPlace>(entity =>
            {
                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.ModifyBy).HasMaxLength(50);

                entity.Property(e => e.Place)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<MasterDataQuestion>(entity =>
            {
                entity.Property(e => e.Code).HasMaxLength(50);

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.ModifyBy).HasMaxLength(50);

                entity.Property(e => e.Name).HasMaxLength(225);
            });

            modelBuilder.Entity<MasterDataRegion>(entity =>
            {
                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ModifyBy)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Region).HasMaxLength(100);
            });

            modelBuilder.Entity<MasterDataResult>(entity =>
            {
                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.JamMasuk).HasMaxLength(10);

                entity.Property(e => e.JamPulang).HasMaxLength(10);

                entity.Property(e => e.ModifyBy)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Nama).HasMaxLength(50);

                entity.Property(e => e.Qrcode)
                    .HasColumnName("QRCode")
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<MasterDataSurvey>(entity =>
            {
                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.ModifyBy)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Question1).HasMaxLength(50);

                entity.Property(e => e.Question10).HasMaxLength(50);

                entity.Property(e => e.Question11).HasMaxLength(50);

                entity.Property(e => e.Question12).HasMaxLength(50);

                entity.Property(e => e.Question13).HasMaxLength(50);

                entity.Property(e => e.Question14).HasMaxLength(50);

                entity.Property(e => e.Question15).HasMaxLength(50);

                entity.Property(e => e.Question16).HasMaxLength(50);

                entity.Property(e => e.Question17).HasMaxLength(50);

                entity.Property(e => e.Question18).HasMaxLength(50);

                entity.Property(e => e.Question2).HasMaxLength(50);

                entity.Property(e => e.Question3).HasMaxLength(50);

                entity.Property(e => e.Question4).HasMaxLength(50);

                entity.Property(e => e.Question5).HasMaxLength(50);

                entity.Property(e => e.Question6).HasMaxLength(50);

                entity.Property(e => e.Question7).HasMaxLength(50);

                entity.Property(e => e.Question8).HasMaxLength(50);

                entity.Property(e => e.Question9).HasMaxLength(50);
            });

            modelBuilder.Entity<MasterDataUser>(entity =>
            {
                entity.Property(e => e.ActivateLink).HasMaxLength(32);

                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.Domain).HasMaxLength(50);

                entity.Property(e => e.ForgetPassword).HasMaxLength(50);

                entity.Property(e => e.Image).HasColumnType("character varying");

                entity.Property(e => e.ModifyBy).HasMaxLength(50);

                entity.Property(e => e.Password).HasMaxLength(50);

                entity.Property(e => e.RegistrationNumber).HasMaxLength(20);

                entity.Property(e => e.UserEmail)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            modelBuilder.Entity<MasterDataUserRole>(entity =>
            {
                entity.Property(e => e.CreatedBy).HasMaxLength(50);

                entity.Property(e => e.ModifyBy).HasMaxLength(50);

                entity.Property(e => e.Role)
                    .IsRequired()
                    .HasMaxLength(50);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
