﻿using SelfAssessment.Model.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SelfAssessment.Repository.Abstract
{
    public interface IFileRepository
    {
        Task<BaseResponse> DownloadFile(string fileName);
    }
}