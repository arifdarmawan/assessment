﻿using SelfAssessment.Model.Models;
using SelfAssessment.Model.Models.Reader;
using System.Threading.Tasks;

namespace SelfAssessment.Repository.Abstract
{
    public interface IScannerRepository
    {
        Task<BaseResponse> UpdateDataResultAfterScanQRCode(ReaderModel data);
    }
}
