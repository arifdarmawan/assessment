﻿using SelfAssessment.Model.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SelfAssessment.Repository.Abstract
{
    public interface IDomainRepository
    {
        Task<IEnumerable<MasterDataDomain>> GetAllDomain();
    }
}
