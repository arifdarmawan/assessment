﻿using SelfAssessment.Model.Models.User;
using System.Collections.Generic;

namespace SelfAssessment.Repository.Abstract
{
    public interface IUserService
    {
        IEnumerable<UserModel> GetAll();
    }
}
