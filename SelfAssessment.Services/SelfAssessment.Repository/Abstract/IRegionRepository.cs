﻿using SelfAssessment.Model.Models;
using System.Threading.Tasks;

namespace SelfAssessment.Repository.Abstract
{
    public interface IRegionRepository
    {
        Task<BaseResponse> GetDataRegion();
    }
}
