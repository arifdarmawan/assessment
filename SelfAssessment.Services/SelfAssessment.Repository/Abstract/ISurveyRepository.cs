﻿using SelfAssessment.Model.Models;
using SelfAssessment.Model.Models.Survey;
using System.Threading.Tasks;

namespace SelfAssessment.Repository.Abstract
{
    public interface ISurveyRepository
    {
        Task<BaseResponse> GetPersonalDatabyUserId(int userid);
        Task<BaseResponse> GetDataSurveybyDateNow(int employeeid);
        Task<BaseResponse> GetQrCode(int userId);
        Task<BaseResponse> InsertDataSurvey(DataSurveyModel model);
    }
}