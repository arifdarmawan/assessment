﻿using SelfAssessment.Model.Models;
using SelfAssessment.Model.Models.Admin;
using System.Threading.Tasks;

namespace SelfAssessment.Repository.Abstract
{
    public interface IResetPasswordRepository
    {
        Task<BaseResponse> SetNewPassword(NewPasswordModel data);
    }
}