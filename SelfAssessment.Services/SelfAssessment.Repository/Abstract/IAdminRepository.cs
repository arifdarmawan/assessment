﻿using SelfAssessment.Model.Models;
using SelfAssessment.Model.Models.Admin;
using System;
using System.Threading.Tasks;

namespace SelfAssessment.Repository.Abstract
{
    public interface IAdminRepository
    {
        Task<BaseResponse> GetTotalResultUserbyColorCode(string userlevel, string region);
        Task<BaseResponse> GetResultUSerSurveybyDateNow(string userlevel, string region);
        Task<BaseResponse> GetResultSurveyByStartAndEndDate(DateTime startDate, DateTime endDate, string userlevel, string region);
        Task<BaseResponse> GetDetailResultSurvey(int userId, DateTime date);
        Task<BaseResponse> GetDataUserAdmin();
        Task<BaseResponse> InsertUserAdmin(UserAdminModel model);
        Task<BaseResponse> UpdateUserAdmin(UserAdminModel model);
        Task<BaseResponse> DeleteUserAdmin(int id);
    }
}