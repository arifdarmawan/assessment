﻿using SelfAssessment.Model.Models;
using System.Threading.Tasks;

namespace SelfAssessment.Repository.Abstract
{
    public interface IKelurahanRepository
    {
        Task<BaseResponse> GetDataKelurahan();
        Task<BaseResponse> GetDataKelurahanByRegionCode(string region);
        Task<BaseResponse> GetDataKelurahanById(int id);
    }
}