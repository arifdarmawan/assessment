﻿using SelfAssessment.Model.Models;
using SelfAssessment.Model.Models.User;
using System.Threading.Tasks;

namespace SelfAssessment.Repository.Abstract
{
    public interface IUserRepository
    {
        Task<BaseResponse> Insert(LoginViewModel data);
        Task<BaseResponse> GetUserByEmail(string email);
        Task<BaseResponse> GetUserByEmailOrUserId(string userid);
        Task<BaseResponse> GetUserById(int id);
        Task<BaseResponse> InsertUserFromAd(LoginViewModel model);
        Task<BaseResponse> UpdateUserFromAd(ActiveDirectoryUserModel data);
        Task<BaseResponse> GetDataUserAD(int userid); 
        Task<BaseResponse> GetEmployeeByUserId(int userid);
        Task<BaseResponse> UpdateEmployee(EmployeeModel model);
    }
}