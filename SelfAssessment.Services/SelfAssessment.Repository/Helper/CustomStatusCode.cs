﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SelfAssessment.Repository.Helper
{
    public enum CustomStatusCode
    {
        UserAdNotRegistered = 600,
        EmployeeDataNotExist = 601,
        InsertFailDataAlreadyExist = 701
    }
}