﻿using SelfAssessment.Model.Models.Admin;
using System.Collections.Generic;

namespace SelfAssessment.Repository.Helper
{
    public class ResultSurvey
    {
        public static List<ResultSurveyModel> CheckQuestionCode(dynamic data,string code, string region)
        {
            List<ResultSurveyModel> resultSurveyModels = new List<ResultSurveyModel>();

            #region mapping
            var question = "";
            if (data.Qcode1 == code)
            {
                resultSurveyModels.Add(new ResultSurveyModel 
                { 
                    Question = data.Q1, Result = code == "1" ? "MERAH" : code == "2" ? "KUNING" : code == "3" ? "HIJAU" : ""
                });;
            }

            if (data.Qcode2 == code)
            {
                resultSurveyModels.Add(new ResultSurveyModel 
                { 
                    Question = data.Q2, Result = code == "1" ? "MERAH" : code == "2" ? "KUNING" : code == "3" ? "HIJAU" : ""
                });
            }

            if (data.Qcode3 == code)
            {
                resultSurveyModels.Add(new ResultSurveyModel 
                { 
                    Question = data.Q3, Result = code == "1" ? "MERAH" : code == "2" ? "KUNING" : code == "3" ? "HIJAU" : ""
                });
            }

            if (data.Qcode5 == code)
            {
                resultSurveyModels.Add(new ResultSurveyModel 
                { 
                    Question = data.Q5, Result = code == "1" ? "MERAH" : code == "2" ? "KUNING" : code == "3" ? "HIJAU" : ""
                });
            }

            if (data.Qcode6 == code)
            {
                if (region == "Jakarta")
                {
                    question = data.Q6;
                }
                else if (region == "Balikpapan")
                {
                    question = "Dalam 14 hari terakhir, apakah Anda menggunakan transportasi umum (Include Grab/Gocar)?";
                }

                resultSurveyModels.Add(new ResultSurveyModel 
                { 
                    Question = question, Result = code == "1" ? "MERAH" : code == "2" ? "KUNING" : code == "3" ? "HIJAU" : ""
                });
            }

            if (data.Qcode7 == code)
            {
                if (region == "Jakarta")
                {
                    question = data.Q7;
                }
                else if (region == "Balikpapan")
                {
                    question = "Dalam 14 hari terakhir, apakah Anda menghadiri acara (dengan jumlah peserta lebih dari 10 orang. Seperti Acara Pernikahan, Seminar, Kitanan, Pesta Perayaan, Mall)?";
                }

                resultSurveyModels.Add(new ResultSurveyModel 
                { 
                    Question = question, Result = code == "1" ? "MERAH" : code == "2" ? "KUNING" : code == "3" ? "HIJAU" : ""
                });
            }

            if (data.Qcode8 == code)
            {
                if (region == "Jakarta")
                {
                    question = data.Q8;
                }
                else if (region == "Balikpapan")
                {
                    question = "Dalam 14 hari terakhir, apakah Anda mengunjungi tempat umum (Pasar/Rumah ibadah/Rumah sakit/minimarket di luar komplek seperti indomaret)?";
                }

                resultSurveyModels.Add(new ResultSurveyModel 
                { 
                    Question = question, Result = code == "1" ? "MERAH" : code == "2" ? "KUNING" : code == "3" ? "HIJAU" : ""
                });
            }

            if (data.Qcode9 == code)
            {
                resultSurveyModels.Add(new ResultSurveyModel 
                { 
                    Question = data.Q9, Result = code == "1" ? "MERAH" : code == "2" ? "KUNING" : code == "3" ? "HIJAU" : ""
                });
            }

            if (data.Qcode10 == code)
            {
                resultSurveyModels.Add(new ResultSurveyModel 
                { 
                    Question = data.Q10, Result = code == "1" ? "MERAH" : code == "2" ? "KUNING" : code == "3" ? "HIJAU" : ""
                });
            }

            if (data.Qcode11 == code)
            {
                resultSurveyModels.Add(new ResultSurveyModel 
                { 
                    Question = data.Q11, Result = code == "1" ? "MERAH" : code == "2" ? "KUNING" : code == "3" ? "HIJAU" : ""
                });
            }

            if (data.Qcode12 == code)
            {
                resultSurveyModels.Add(new ResultSurveyModel 
                { 
                    Question = data.Q12, Result = code == "1" ? "MERAH" : code == "2" ? "KUNING" : code == "3" ? "HIJAU" : ""
                });
            }

            if (data.Qcode13 == code)
            {
                resultSurveyModels.Add(new ResultSurveyModel 
                { 
                    Question = data.Q13, Result = code == "1" ? "MERAH" : code == "2" ? "KUNING" : code == "3" ? "HIJAU" : ""
                });
            }

            if (data.Qcode14 == code)
            {
                resultSurveyModels.Add(new ResultSurveyModel 
                { 
                    Question = data.Q14, Result = code == "1" ? "MERAH" : code == "2" ? "KUNING" : code == "3" ? "HIJAU" : ""
                });
            }

            if (data.Qcode15 == code)
            {
                if (region == "Jakarta")
                {
                    question = data.Q15;
                }
                else if (region == "Balikpapan")
                {
                    question = "Apakah Anda dalam 30 hari terakhir, melakukan perjalanan dari luar KALIMANTAN TIMUR? (Perjalan Dinas/Cuti)";
                }

                resultSurveyModels.Add(new ResultSurveyModel 
                { 
                    Question = question, Result = code == "1" ? "MERAH" : code == "2" ? "KUNING" : code == "3" ? "HIJAU" : ""
                });
            }

            if (data.Qcode16 == code)
            {
                resultSurveyModels.Add(new ResultSurveyModel 
                { 
                    Question = data.Q16, Result = code == "1" ? "MERAH" : code == "2" ? "KUNING" : code == "3" ? "HIJAU" : ""
                });
            }

            if (data.Qcode17 == code)
            {
                resultSurveyModels.Add(new ResultSurveyModel 
                { 
                    Question = data.Q17, Result = code == "1" ? "MERAH" : code == "2" ? "KUNING" : code == "3" ? "HIJAU" : ""
                });
            }

            if (data.Qcode18 == code)
            {
                resultSurveyModels.Add(new ResultSurveyModel 
                { 
                    Question = data.Q18, Result = code == "1" ? "MERAH" : code == "2" ? "KUNING" : code == "3" ? "HIJAU" : ""
                });
            }
            #endregion

            return resultSurveyModels;
        }
    }
}