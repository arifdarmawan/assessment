﻿using System.Collections.Generic;

namespace SelfAssessment.Repository.Helper
{
    public class ResultCode
    {
        #region JAKARTA
        public static int ResultCodeJakarta(dynamic model)
        {
            int result = 0;
            var resultassesmentself = ColorCodeJakarta(model);

            var resultred = resultassesmentself.Contains("RED");
            if (resultred == true)
            {
                return result = 1;
            }

            var resultyellow = resultassesmentself.Contains("YELLOW");
            if (resultyellow == true)
            {
                return result = 2;
            }

            var resultgreen = resultassesmentself.Contains("GREEN");
            if (resultgreen == true)
            {
                return result = 3;
            }

            return result;
        }

        private static List<string> ColorCodeJakarta(dynamic data)
        {
            var dictionary = new Dictionary<string, string>();


            if (data.Question1 == "2")
            {
                dictionary.Add("Question1", "YELLOW");
            }
            else
            {
                dictionary.Add("Question1", "GREEN");
            }

            if (data.Question2 == "3")
            {
                dictionary.Add("Question2", "GREEN");
            }
            else
            {
                dictionary.Add("Question2", "YELLOW");
            }

            if (data.Question3 == "2")
            {
                dictionary.Add("Question3", "YELLOW");
            }
            else
            {
                dictionary.Add("Question3", "GREEN");
            }

            if (data.Question5 == "3")
            {
                dictionary.Add("Question5", "GREEN");
            }
            else
            {
                dictionary.Add("Question5", "RED");
            }

            if (data.Question6 == "1")
            {
                dictionary.Add("Question6", "RED");
            }
            else
            {
                dictionary.Add("Question6", "GREEN");
            }

            if (data.Question7 == "1")
            {
                dictionary.Add("Question7", "RED");
            }
            else
            {
                dictionary.Add("Question7", "GREEN");
            }

            if (data.Question8 == "2")
            {
                dictionary.Add("Question8", "YELLOW");
            }
            else
            {
                dictionary.Add("Question8", "GREEN");
            }

            if (data.Question9 == "1")
            {
                dictionary.Add("Question9", "RED");
            }
            else
            {
                dictionary.Add("Question9", "GREEN");
            }

            if (data.Question10 == "2")
            {
                dictionary.Add("Question10", "YELLOW");
            }
            else
            {
                dictionary.Add("Question10", "GREEN");
            }

            if (data.Question11 == "1")
            {
                dictionary.Add("Question11", "RED");
            }
            else
            {
                dictionary.Add("Question11", "GREEN");
            }

            if (data.Question12 == "1")
            {
                dictionary.Add("Question12", "RED");
            }
            else
            {
                dictionary.Add("Question12", "GREEN");
            }

            if (data.Question13 == "2")
            {
                dictionary.Add("Question13", "YELLOW");
            }
            else
            {
                dictionary.Add("Question13", "GREEN");
            }

            if (data.Question14 == "2")
            {
                dictionary.Add("Question14", "YELLOW");
            }
            else
            {
                dictionary.Add("Question14", "GREEN");
            }

            if (data.Question15 == "1")
            {
                dictionary.Add("Question15", "RED");
            }
            else
            {
                dictionary.Add("Question15", "GREEN");
            }

            if (data.Question16 == "1")
            {
                dictionary.Add("Question16", "RED");

            }
            else
            {
                dictionary.Add("Question16", "GREEN");
            }

            if (data.Question17 == "1")
            {
                dictionary.Add("Question17", "RED");

            }
            else
            {
                dictionary.Add("Question17", "GREEN");
            }

            if (data.Question18 == "1")
            {
                dictionary.Add("Question18", "RED");

            }
            else
            {
                dictionary.Add("Question18", "GREEN");
            }

            var listsurveyassesment = new List<string>(dictionary.Values);

            return listsurveyassesment;
        }
        #endregion

        #region BALIKPAPAN
        public static int ResultCodeBalikpapan(dynamic model)
        {
            int result = 0;
            var resultassesmentself = ColorCodeBalikpapan(model);

            var resultred = resultassesmentself.Contains("RED");
            if (resultred == true)
            {
                return result = 1;
            }

            var resultyellow = resultassesmentself.Contains("YELLOW");
            if (resultyellow == true)
            {
                return result = 2;
            }

            var resultgreen = resultassesmentself.Contains("GREEN");
            if (resultgreen == true)
            {
                return result = 3;
            }

            return result;
        }

        private static List<string> ColorCodeBalikpapan(dynamic data)
        {
            var dictionary = new Dictionary<string, string>();


            if (data.Question1 == "2")
            {
                dictionary.Add("Question1", "YELLOW");
            }
            else
            {
                dictionary.Add("Question1", "GREEN");
            }

            if (data.Question2 == "3")
            {
                dictionary.Add("Question2", "GREEN");
            }
            else
            {
                dictionary.Add("Question2", "YELLOW");
            }

            if (data.Question3 == "2")
            {
                dictionary.Add("Question3", "YELLOW");
            }
            else
            {
                dictionary.Add("Question3", "GREEN");
            }

            if (data.Question5 == "3")
            {
                dictionary.Add("Question5", "GREEN");
            }
            else
            {
                dictionary.Add("Question5", "RED");
            }

            if (data.Question6 == "1")
            {
                dictionary.Add("Question6", "RED");
            }
            else
            {
                dictionary.Add("Question6", "GREEN");
            }

            if (data.Question7 == "1")
            {
                dictionary.Add("Question7", "RED");
            }
            else
            {
                dictionary.Add("Question7", "GREEN");
            }

            if (data.Question8 == "2")
            {
                dictionary.Add("Question8", "YELLOW");
            }
            else
            {
                dictionary.Add("Question8", "GREEN");
            }

            if (data.Question9 == "1")
            {
                dictionary.Add("Question9", "RED");
            }
            else
            {
                dictionary.Add("Question9", "GREEN");
            }

            if (data.Question10 == "2")
            {
                dictionary.Add("Question10", "YELLOW");
            }
            else
            {
                dictionary.Add("Question10", "GREEN");
            }

            if (data.Question11 == "1")
            {
                dictionary.Add("Question11", "RED");
            }
            else
            {
                dictionary.Add("Question11", "GREEN");
            }

            if (data.Question12 == "1")
            {
                dictionary.Add("Question12", "RED");
            }
            else
            {
                dictionary.Add("Question12", "GREEN");
            }

            if (data.Question13 == "2")
            {
                dictionary.Add("Question13", "YELLOW");
            }
            else
            {
                dictionary.Add("Question13", "GREEN");
            }

            if (data.Question14 == "1")
            {
                dictionary.Add("Question14", "RED");
            }
            else
            {
                dictionary.Add("Question14", "GREEN");
            }

            if (data.Question15 == "2")
            {
                dictionary.Add("Question15", "YELLOW");
            }
            else
            {
                dictionary.Add("Question15", "GREEN");
            }

            if (data.Question16 == "2")
            {
                dictionary.Add("Question16", "YELLOW");

            }
            else
            {
                dictionary.Add("Question16", "GREEN");
            }

            if (data.Question17 == "1")
            {
                dictionary.Add("Question17", "RED");

            }
            else
            {
                dictionary.Add("Question17", "GREEN");
            }

            if (data.Question18 == "1")
            {
                dictionary.Add("Question18", "RED");

            }
            else
            {
                dictionary.Add("Question18", "GREEN");
            }

            var listsurveyassesment = new List<string>(dictionary.Values);

            return listsurveyassesment;
        }
        #endregion
    }
}