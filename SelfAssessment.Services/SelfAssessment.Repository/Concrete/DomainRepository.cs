﻿using Microsoft.EntityFrameworkCore;
using SelfAssessment.Model.Entities;
using SelfAssessment.Repository.Abstract;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SelfAssessment.Repository.Concrete
{
    public class DomainRepository : IDomainRepository
    {
        readonly SelfAssessmentContext _context;
        public DomainRepository(SelfAssessmentContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<MasterDataDomain>> GetAllDomain()
        {
            return await _context.MasterDataDomain.AsNoTracking().ToListAsync();
        }
    }
}
