﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using SelfAssessment.Model.Entities;
using SelfAssessment.Model.Models;
using SelfAssessment.Model.Models.Admin;
using SelfAssessment.Repository.Abstract;
using SelfAssessment.Repository.Helper;

namespace SelfAssessment.Repository.Concrete
{
    public class AdminRepository : IAdminRepository
    {
        readonly SelfAssessmentContext _context;
        public AdminRepository(SelfAssessmentContext context)
        {
            _context = context;
        }

        public async Task<BaseResponse> GetTotalResultUserbyColorCode(string userlevel, string region)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                DashboardModel model = new DashboardModel();

                if (userlevel != "SUPERADMIN")
                {
                    model.TotalWarnaMerah = await (from res in _context.MasterDataResult
                                                   join emp in _context.MasterDataEmployee
                                                        on res.MasterDataUserId equals emp.MasterDataUserId
                                                   where res.ColorCode == 1 && res.CreatedDate.Date == DateTime.Now.Date
                                                       && emp.RegionOffice == region
                                                   select new
                                                   {
                                                       res.ColorCode
                                                   }).CountAsync();

                    model.TotalWarnaKuning = await (from res in _context.MasterDataResult
                                                    join emp in _context.MasterDataEmployee
                                                         on res.MasterDataUserId equals emp.MasterDataUserId
                                                    where res.ColorCode == 2 && res.CreatedDate.Date == DateTime.Now.Date
                                                        && emp.RegionOffice == region
                                                    select new
                                                    {
                                                        res.ColorCode
                                                    }).CountAsync();

                    model.TotalWarnaHijau = await (from res in _context.MasterDataResult
                                                   join emp in _context.MasterDataEmployee
                                                        on res.MasterDataUserId equals emp.MasterDataUserId
                                                   where res.ColorCode == 3 && res.CreatedDate.Date == DateTime.Now.Date
                                                        && emp.RegionOffice == region
                                                   select new
                                                   {
                                                       res.ColorCode
                                                   }).CountAsync();
                }
                else 
                {
                    model.TotalWarnaMerah = await (from res in _context.MasterDataResult
                                                   join emp in _context.MasterDataEmployee
                                                        on res.MasterDataUserId equals emp.MasterDataUserId
                                                   where res.ColorCode == 1 && res.CreatedDate.Date == DateTime.Now.Date
                                                   select new
                                                   {
                                                       res.ColorCode
                                                   }).CountAsync();

                    model.TotalWarnaKuning = await (from res in _context.MasterDataResult
                                                    join emp in _context.MasterDataEmployee
                                                         on res.MasterDataUserId equals emp.MasterDataUserId
                                                    where res.ColorCode == 2 && res.CreatedDate.Date == DateTime.Now.Date
                                                    select new
                                                    {
                                                        res.ColorCode
                                                    }).CountAsync();

                    model.TotalWarnaHijau = await (from res in _context.MasterDataResult
                                                   join emp in _context.MasterDataEmployee
                                                        on res.MasterDataUserId equals emp.MasterDataUserId
                                                   where res.ColorCode == 3 && res.CreatedDate.Date == DateTime.Now.Date
                                                   select new
                                                   {
                                                       res.ColorCode
                                                   }).CountAsync();
                }

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (model != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = model;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetResultUSerSurveybyDateNow(string userlevel, string region)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                if (userlevel != "SUPERADMIN")
                {
                    var result = await (from emp in _context.MasterDataEmployee
                                        join res in _context.MasterDataResult
                                            on emp.MasterDataUserId equals res.MasterDataUserId
                                        join mapping in _context.MasterDataMappingSurveyQuestion
                                           on new
                                           {
                                               res.MasterDataUserId,
                                               res.CreatedDate.Date
                                           }
                                           equals new
                                           {
                                               mapping.MasterDataUserId,
                                               mapping.CreatedDate.Date
                                           }
                                        join kel in _context.MasterDataKelurahan
                                            on emp.MasterDataKelurahanId equals kel.Id
                                        where res.CreatedDate.Date == DateTime.Now.Date
                                            && emp.RegionOffice == region
                                        select new
                                        {
                                            res.Id,
                                            emp.MasterDataUserId,
                                            Tanggal = res.CreatedDate,
                                            NIK = emp.Nik,
                                            Name = emp.EmployeeName,
                                            emp.Email,
                                            emp.RegionOffice,
                                            emp.Divisi,
                                            Gender = emp.Gender == 1 ? "Pria" : "Wanita",
                                            emp.Address,
                                            kel.Kelurahan,
                                            ColorCode = res.ColorCode == 1 ? "Merah" : res.ColorCode == 2 ? "Kuning" : res.ColorCode == 3 ? "Hijau" : "",
                                            res.JamMasuk,
                                            res.JamPulang,
                                            Q1 = mapping.Qcode1 == "1" ? "Merah" : mapping.Qcode1 == "2" ? "Kuning" : mapping.Qcode1 == "3" ? "Hijau" : "",
                                            Q2 = mapping.Qcode2 == "1" ? "Merah" : mapping.Qcode2 == "2" ? "Kuning" : mapping.Qcode2 == "3" ? "Hijau" : "",
                                            Q3 = mapping.Qcode3 == "1" ? "Merah" : mapping.Qcode3 == "2" ? "Kuning" : mapping.Qcode3 == "3" ? "Hijau" : "",
                                            Q4 = "",
                                            Q5 = mapping.Qcode5 == "1" ? "Merah" : mapping.Qcode5 == "2" ? "Kuning" : mapping.Qcode5 == "3" ? "Hijau" : "",
                                            Q6 = mapping.Qcode6 == "1" ? "Merah" : mapping.Qcode6 == "2" ? "Kuning" : mapping.Qcode6 == "3" ? "Hijau" : "",
                                            Q7 = mapping.Qcode7 == "1" ? "Merah" : mapping.Qcode7 == "2" ? "Kuning" : mapping.Qcode7 == "3" ? "Hijau" : "",
                                            Q8 = mapping.Qcode8 == "1" ? "Merah" : mapping.Qcode8 == "2" ? "Kuning" : mapping.Qcode8 == "3" ? "Hijau" : "",
                                            Q9 = mapping.Qcode9 == "1" ? "Merah" : mapping.Qcode9 == "2" ? "Kuning" : mapping.Qcode9 == "3" ? "Hijau" : "",
                                            Q10 = mapping.Qcode10 == "1" ? "Merah" : mapping.Qcode10 == "2" ? "Kuning" : mapping.Qcode10 == "3" ? "Hijau" : "",
                                            Q11 = mapping.Qcode11 == "1" ? "Merah" : mapping.Qcode11 == "2" ? "Kuning" : mapping.Qcode11 == "3" ? "Hijau" : "",
                                            Q12 = mapping.Qcode12 == "1" ? "Merah" : mapping.Qcode12 == "2" ? "Kuning" : mapping.Qcode12 == "3" ? "Hijau" : "",
                                            Q13 = mapping.Qcode13 == "1" ? "Merah" : mapping.Qcode13 == "2" ? "Kuning" : mapping.Qcode13 == "3" ? "Hijau" : "",
                                            Q14 = mapping.Qcode14 == "1" ? "Merah" : mapping.Qcode14 == "2" ? "Kuning" : mapping.Qcode14 == "3" ? "Hijau" : "",
                                            Q15 = mapping.Qcode15 == "1" ? "Merah" : mapping.Qcode15 == "2" ? "Kuning" : mapping.Qcode15 == "3" ? "Hijau" : "",
                                            Q16 = mapping.Qcode16 == "1" ? "Merah" : mapping.Qcode16 == "2" ? "Kuning" : mapping.Qcode16 == "3" ? "Hijau" : "",
                                            Q17 = mapping.Qcode17 == "1" ? "Merah" : mapping.Qcode17 == "2" ? "Kuning" : mapping.Qcode17 == "3" ? "Hijau" : "",
                                            Q18 = mapping.Qcode18 == "1" ? "Merah" : mapping.Qcode18 == "2" ? "Kuning" : mapping.Qcode18 == "3" ? "Hijau" : "",
                                        }).AsNoTracking().ToListAsync();

                    if (result != null)
                    {
                        response.Message = "Retrieve data success";
                        response.Status = HttpStatusCode.OK.ToString();
                        response.Code = (int)HttpStatusCode.OK;
                        response.Data = result;
                    }
                    else
                    {
                        response.Message = "No result";
                        response.Status = HttpStatusCode.OK.ToString();
                        response.Code = (int)HttpStatusCode.OK;
                    }
                }
                else 
                {
                    var result = await (from emp in _context.MasterDataEmployee
                                        join res in _context.MasterDataResult
                                            on emp.MasterDataUserId equals res.MasterDataUserId
                                        join mapping in _context.MasterDataMappingSurveyQuestion
                                           on new
                                           {
                                               res.MasterDataUserId,
                                               res.CreatedDate.Date
                                           }
                                           equals new
                                           {
                                               mapping.MasterDataUserId,
                                               mapping.CreatedDate.Date
                                           }
                                        join kel in _context.MasterDataKelurahan
                                            on emp.MasterDataKelurahanId equals kel.Id
                                        where res.CreatedDate.Date == DateTime.Now.Date
                                        select new
                                        {
                                            res.Id,
                                            emp.MasterDataUserId,
                                            Tanggal = res.CreatedDate,
                                            NIK = emp.Nik,
                                            Name = emp.EmployeeName,
                                            emp.Email,
                                            emp.RegionOffice,
                                            emp.Divisi,
                                            Gender = emp.Gender == 1 ? "Pria" : "Wanita",
                                            emp.Address,
                                            kel.Kelurahan,
                                            ColorCode = res.ColorCode == 1 ? "Merah" : res.ColorCode == 2 ? "Kuning" : res.ColorCode == 3 ? "Hijau" : "",
                                            res.JamMasuk,
                                            res.JamPulang,
                                            Q1 = mapping.Qcode1 == "1" ? "Merah" : mapping.Qcode1 == "2" ? "Kuning" : mapping.Qcode1 == "3" ? "Hijau" : "",
                                            Q2 = mapping.Qcode2 == "1" ? "Merah" : mapping.Qcode2 == "2" ? "Kuning" : mapping.Qcode2 == "3" ? "Hijau" : "",
                                            Q3 = mapping.Qcode3 == "1" ? "Merah" : mapping.Qcode3 == "2" ? "Kuning" : mapping.Qcode3 == "3" ? "Hijau" : "",
                                            Q4 = "",
                                            Q5 = mapping.Qcode5 == "1" ? "Merah" : mapping.Qcode5 == "2" ? "Kuning" : mapping.Qcode5 == "3" ? "Hijau" : "",
                                            Q6 = mapping.Qcode6 == "1" ? "Merah" : mapping.Qcode6 == "2" ? "Kuning" : mapping.Qcode6 == "3" ? "Hijau" : "",
                                            Q7 = mapping.Qcode7 == "1" ? "Merah" : mapping.Qcode7 == "2" ? "Kuning" : mapping.Qcode7 == "3" ? "Hijau" : "",
                                            Q8 = mapping.Qcode8 == "1" ? "Merah" : mapping.Qcode8 == "2" ? "Kuning" : mapping.Qcode8 == "3" ? "Hijau" : "",
                                            Q9 = mapping.Qcode9 == "1" ? "Merah" : mapping.Qcode9 == "2" ? "Kuning" : mapping.Qcode9 == "3" ? "Hijau" : "",
                                            Q10 = mapping.Qcode10 == "1" ? "Merah" : mapping.Qcode10 == "2" ? "Kuning" : mapping.Qcode10 == "3" ? "Hijau" : "",
                                            Q11 = mapping.Qcode11 == "1" ? "Merah" : mapping.Qcode11 == "2" ? "Kuning" : mapping.Qcode11 == "3" ? "Hijau" : "",
                                            Q12 = mapping.Qcode12 == "1" ? "Merah" : mapping.Qcode12 == "2" ? "Kuning" : mapping.Qcode12 == "3" ? "Hijau" : "",
                                            Q13 = mapping.Qcode13 == "1" ? "Merah" : mapping.Qcode13 == "2" ? "Kuning" : mapping.Qcode13 == "3" ? "Hijau" : "",
                                            Q14 = mapping.Qcode14 == "1" ? "Merah" : mapping.Qcode14 == "2" ? "Kuning" : mapping.Qcode14 == "3" ? "Hijau" : "",
                                            Q15 = mapping.Qcode15 == "1" ? "Merah" : mapping.Qcode15 == "2" ? "Kuning" : mapping.Qcode15 == "3" ? "Hijau" : "",
                                            Q16 = mapping.Qcode16 == "1" ? "Merah" : mapping.Qcode16 == "2" ? "Kuning" : mapping.Qcode16 == "3" ? "Hijau" : "",
                                            Q17 = mapping.Qcode17 == "1" ? "Merah" : mapping.Qcode17 == "2" ? "Kuning" : mapping.Qcode17 == "3" ? "Hijau" : "",
                                            Q18 = mapping.Qcode18 == "1" ? "Merah" : mapping.Qcode18 == "2" ? "Kuning" : mapping.Qcode18 == "3" ? "Hijau" : "",
                                        }).AsNoTracking().ToListAsync();
                    if (result != null)
                    {
                        response.Message = "Retrieve data success";
                        response.Status = HttpStatusCode.OK.ToString();
                        response.Code = (int)HttpStatusCode.OK;
                        response.Data = result;
                    }
                    else
                    {
                        response.Message = "No result"; 
                        response.Status = HttpStatusCode.OK.ToString();
                        response.Code = (int)HttpStatusCode.OK;
                    }
                }
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetResultSurveyByStartAndEndDate(DateTime startDate, DateTime endDate, string userlevel, string region)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                if (userlevel != "SUPERADMIN")
                {
                    var result = await (from emp in _context.MasterDataEmployee
                                        join res in _context.MasterDataResult
                                            on emp.MasterDataUserId equals res.MasterDataUserId
                                        join mapping in _context.MasterDataMappingSurveyQuestion
                                           on new
                                           {
                                               res.MasterDataUserId,
                                               res.CreatedDate.Date
                                           } equals new
                                           {
                                               mapping.MasterDataUserId,
                                               mapping.CreatedDate.Date
                                           }
                                        join kel in _context.MasterDataKelurahan
                                            on emp.MasterDataKelurahanId equals kel.Id
                                        where (res.CreatedDate.Date >= startDate.Date && res.CreatedDate.Date <= endDate.Date)
                                            && emp.RegionOffice == region
                                        select new
                                        {
                                            res.Id,
                                            emp.MasterDataUserId,
                                            Tanggal = res.CreatedDate,
                                            NIK = emp.Nik,
                                            Name = emp.EmployeeName,
                                            emp.Email,
                                            emp.RegionOffice,
                                            emp.Divisi,
                                            Gender = emp.Gender == 1 ? "Pria" : "Wanita",
                                            emp.Address,
                                            kel.Kelurahan,
                                            ColorCode = res.ColorCode == 1 ? "Merah" : res.ColorCode == 2 ? "Kuning" : res.ColorCode == 3 ? "Hijau" : "",
                                            res.JamMasuk,
                                            res.JamPulang,
                                            Q1 = mapping.Qcode1 == "1" ? "Merah" : mapping.Qcode1 == "2" ? "Kuning" : mapping.Qcode1 == "3" ? "Hijau" : "",
                                            Q2 = mapping.Qcode2 == "1" ? "Merah" : mapping.Qcode2 == "2" ? "Kuning" : mapping.Qcode2 == "3" ? "Hijau" : "",
                                            Q3 = mapping.Qcode3 == "1" ? "Merah" : mapping.Qcode3 == "2" ? "Kuning" : mapping.Qcode3 == "3" ? "Hijau" : "",
                                            Q4 = "",
                                            Q5 = mapping.Qcode5 == "1" ? "Merah" : mapping.Qcode5 == "2" ? "Kuning" : mapping.Qcode5 == "3" ? "Hijau" : "",
                                            Q6 = mapping.Qcode6 == "1" ? "Merah" : mapping.Qcode6 == "2" ? "Kuning" : mapping.Qcode6 == "3" ? "Hijau" : "",
                                            Q7 = mapping.Qcode7 == "1" ? "Merah" : mapping.Qcode7 == "2" ? "Kuning" : mapping.Qcode7 == "3" ? "Hijau" : "",
                                            Q8 = mapping.Qcode8 == "1" ? "Merah" : mapping.Qcode8 == "2" ? "Kuning" : mapping.Qcode8 == "3" ? "Hijau" : "",
                                            Q9 = mapping.Qcode9 == "1" ? "Merah" : mapping.Qcode9 == "2" ? "Kuning" : mapping.Qcode9 == "3" ? "Hijau" : "",
                                            Q10 = mapping.Qcode10 == "1" ? "Merah" : mapping.Qcode10 == "2" ? "Kuning" : mapping.Qcode10 == "3" ? "Hijau" : "",
                                            Q11 = mapping.Qcode11 == "1" ? "Merah" : mapping.Qcode11 == "2" ? "Kuning" : mapping.Qcode11 == "3" ? "Hijau" : "",
                                            Q12 = mapping.Qcode12 == "1" ? "Merah" : mapping.Qcode12 == "2" ? "Kuning" : mapping.Qcode12 == "3" ? "Hijau" : "",
                                            Q13 = mapping.Qcode13 == "1" ? "Merah" : mapping.Qcode13 == "2" ? "Kuning" : mapping.Qcode13 == "3" ? "Hijau" : "",
                                            Q14 = mapping.Qcode14 == "1" ? "Merah" : mapping.Qcode14 == "2" ? "Kuning" : mapping.Qcode14 == "3" ? "Hijau" : "",
                                            Q15 = mapping.Qcode15 == "1" ? "Merah" : mapping.Qcode15 == "2" ? "Kuning" : mapping.Qcode15 == "3" ? "Hijau" : "",
                                            Q16 = mapping.Qcode16 == "1" ? "Merah" : mapping.Qcode16 == "2" ? "Kuning" : mapping.Qcode16 == "3" ? "Hijau" : "",
                                            Q17 = mapping.Qcode17 == "1" ? "Merah" : mapping.Qcode17 == "2" ? "Kuning" : mapping.Qcode17 == "3" ? "Hijau" : "",
                                            Q18 = mapping.Qcode18 == "1" ? "Merah" : mapping.Qcode18 == "2" ? "Kuning" : mapping.Qcode18 == "3" ? "Hijau" : "",
                                        }).AsNoTracking().ToListAsync();

                    if (result != null)
                    {
                        response.Message = "Retrieve data success";
                        response.Status = HttpStatusCode.OK.ToString();
                        response.Code = (int)HttpStatusCode.OK;
                        response.Data = result;
                    }
                    else
                    {
                        response.Message = "No result";
                        response.Status = HttpStatusCode.OK.ToString();
                        response.Code = (int)HttpStatusCode.OK;
                    }
                }
                else
                {
                    var result = await (from emp in _context.MasterDataEmployee
                                        join res in _context.MasterDataResult
                                            on emp.MasterDataUserId equals res.MasterDataUserId
                                        join mapping in _context.MasterDataMappingSurveyQuestion
                                           on new
                                           {
                                               res.MasterDataUserId,
                                               res.CreatedDate.Date
                                           } equals new
                                           {
                                               mapping.MasterDataUserId,
                                               mapping.CreatedDate.Date
                                           }
                                        join kel in _context.MasterDataKelurahan
                                            on emp.MasterDataKelurahanId equals kel.Id
                                        where (res.CreatedDate.Date >= startDate.Date && res.CreatedDate.Date <= endDate.Date)
                                        select new
                                        {
                                            res.Id,
                                            emp.MasterDataUserId,
                                            Tanggal = res.CreatedDate,
                                            NIK = emp.Nik,
                                            Name = emp.EmployeeName,
                                            emp.Email,
                                            emp.RegionOffice,
                                            emp.Divisi,
                                            Gender = emp.Gender == 1 ? "Pria" : "Wanita",
                                            emp.Address,
                                            kel.Kelurahan,
                                            ColorCode = res.ColorCode == 1 ? "Merah" : res.ColorCode == 2 ? "Kuning" : res.ColorCode == 3 ? "Hijau" : "",
                                            res.JamMasuk,
                                            res.JamPulang,
                                            Q1 = mapping.Qcode1 == "1" ? "Merah" : mapping.Qcode1 == "2" ? "Kuning" : mapping.Qcode1 == "3" ? "Hijau" : "",
                                            Q2 = mapping.Qcode2 == "1" ? "Merah" : mapping.Qcode2 == "2" ? "Kuning" : mapping.Qcode2 == "3" ? "Hijau" : "",
                                            Q3 = mapping.Qcode3 == "1" ? "Merah" : mapping.Qcode3 == "2" ? "Kuning" : mapping.Qcode3 == "3" ? "Hijau" : "",
                                            Q4 = "",
                                            Q5 = mapping.Qcode5 == "1" ? "Merah" : mapping.Qcode5 == "2" ? "Kuning" : mapping.Qcode5 == "3" ? "Hijau" : "",
                                            Q6 = mapping.Qcode6 == "1" ? "Merah" : mapping.Qcode6 == "2" ? "Kuning" : mapping.Qcode6 == "3" ? "Hijau" : "",
                                            Q7 = mapping.Qcode7 == "1" ? "Merah" : mapping.Qcode7 == "2" ? "Kuning" : mapping.Qcode7 == "3" ? "Hijau" : "",
                                            Q8 = mapping.Qcode8 == "1" ? "Merah" : mapping.Qcode8 == "2" ? "Kuning" : mapping.Qcode8 == "3" ? "Hijau" : "",
                                            Q9 = mapping.Qcode9 == "1" ? "Merah" : mapping.Qcode9 == "2" ? "Kuning" : mapping.Qcode9 == "3" ? "Hijau" : "",
                                            Q10 = mapping.Qcode10 == "1" ? "Merah" : mapping.Qcode10 == "2" ? "Kuning" : mapping.Qcode10 == "3" ? "Hijau" : "",
                                            Q11 = mapping.Qcode11 == "1" ? "Merah" : mapping.Qcode11 == "2" ? "Kuning" : mapping.Qcode11 == "3" ? "Hijau" : "",
                                            Q12 = mapping.Qcode12 == "1" ? "Merah" : mapping.Qcode12 == "2" ? "Kuning" : mapping.Qcode12 == "3" ? "Hijau" : "",
                                            Q13 = mapping.Qcode13 == "1" ? "Merah" : mapping.Qcode13 == "2" ? "Kuning" : mapping.Qcode13 == "3" ? "Hijau" : "",
                                            Q14 = mapping.Qcode14 == "1" ? "Merah" : mapping.Qcode14 == "2" ? "Kuning" : mapping.Qcode14 == "3" ? "Hijau" : "",
                                            Q15 = mapping.Qcode15 == "1" ? "Merah" : mapping.Qcode15 == "2" ? "Kuning" : mapping.Qcode15 == "3" ? "Hijau" : "",
                                            Q16 = mapping.Qcode16 == "1" ? "Merah" : mapping.Qcode16 == "2" ? "Kuning" : mapping.Qcode16 == "3" ? "Hijau" : "",
                                            Q17 = mapping.Qcode17 == "1" ? "Merah" : mapping.Qcode17 == "2" ? "Kuning" : mapping.Qcode17 == "3" ? "Hijau" : "",
                                            Q18 = mapping.Qcode18 == "1" ? "Merah" : mapping.Qcode18 == "2" ? "Kuning" : mapping.Qcode18 == "3" ? "Hijau" : "",
                                        }).AsNoTracking().ToListAsync();

                    if (result != null)
                    {
                        response.Message = "Retrieve data success";
                        response.Status = HttpStatusCode.OK.ToString();
                        response.Code = (int)HttpStatusCode.OK;
                        response.Data = result;
                    }
                    else
                    {
                        response.Message = "No result";
                        response.Status = HttpStatusCode.OK.ToString();
                        response.Code = (int)HttpStatusCode.OK;
                    }
                }
            }
            catch(Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetDetailResultSurvey(int userId, DateTime date) 
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from res in _context.MasterDataResult
                                        join emp in _context.MasterDataEmployee
                                        on res.MasterDataUserId equals emp.MasterDataUserId
                                    where res.MasterDataUserId == userId
                                    && res.CreatedDate.Date == date.Date
                                    select new 
                                    {
                                        res.ColorCode,
                                        emp.RegionOffice
                                    }).FirstOrDefaultAsync();

                if (result != null)
                {

                    var mappingSurveyQuestion = await (from surveyquestion in _context.MasterDataMappingSurveyQuestion
                                                       where surveyquestion.MasterDataUserId == userId
                                                       && surveyquestion.CreatedDate.Date == date.Date
                                                       select surveyquestion).FirstOrDefaultAsync();
                    string code = Convert.ToString(result.ColorCode);
                    var resSurvey = ResultSurvey.CheckQuestionCode(mappingSurveyQuestion, code, result.RegionOffice);

                    response.Data = resSurvey;
                    response.Message = "Retrieve data success";
                }
                else 
                {
                    response.Message = "No result";
                }
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }
            
            return response;
        }

        public async Task<BaseResponse> GetDataUserAdmin() 
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from user in _context.MasterDataUser
                                    join emp in _context.MasterDataEmployee
                                        on user.Id equals emp.MasterDataUserId
                                    where user.UserLevel != 2 &&  user.IsDelete != true
                                    select new
                                    {
                                        user.Id,
                                        user.UserId,
                                        UserLevel = user.UserLevel == 1 ? "Super Admin" : user.UserLevel == 3 ? "Admin" : "",
                                        Region = emp.RegionOffice,
                                        UserName = emp.EmployeeName,
                                        user.CreatedBy,
                                        user.CreatedDate,
                                    }).AsNoTracking().ToListAsync();

                if (result != null)
                {    
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> InsertUserAdmin(UserAdminModel model) 
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    #region USER
                    MasterDataUser newUser = new MasterDataUser
                    {
                        UserId = model.UserId,
                        Password = model.Password,
                        UserEmail = model.UserId + "@first-resources.com",
                        UserLevel = model.UserRole,
                        CreatedBy = model.CreatedBy,
                        CreatedDate = DateTime.Now,
                        ModifyBy = model.ModifyBy,
                        ModifyDate = DateTime.Now,
                        IsActive = true,
                        IsDelete = false
                    };

                    await _context.MasterDataUser.AddAsync(newUser);
                    await _context.SaveChangesAsync();
                    #endregion

                    #region EMPLOYEE
                    var datauser = await _context.MasterDataEmployee.FirstOrDefaultAsync(n => n.MasterDataUserId == newUser.Id);

                    if (datauser == null)
                    {
                        MasterDataEmployee employee = new MasterDataEmployee
                        {
                            EmployeeName = model.UserName,
                            MasterDataUserId = newUser.Id,
                            MasterDataKelurahanId = 0,
                            Gender = 0,
                            Email = model.UserId + "@first-resources.com",
                            RegionOffice = model.Region,
                            IsActive = true,
                            CreatedBy = model.UserId,
                            CreatedDate = DateTime.Now,
                            ModifyBy = model.UserId,
                            ModifyDate = DateTime.Now
                        };

                        await _context.MasterDataEmployee.AddAsync(employee);
                        await _context.SaveChangesAsync();
                    }
                    #endregion

                    dbcxtransaction.Commit();

                    response.Code = (int)HttpStatusCode.OK;
                    response.Status = HttpStatusCode.OK.ToString();
                    response.Message = "successfull insert data user.!";
                    response.Data = newUser;
                }
                catch (Exception ex)
                {
                    response.Message = ex.Message.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }
            return response;
        }

        public async Task<BaseResponse> UpdateUserAdmin(UserAdminModel model)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    #region User
                    var user = await _context.MasterDataUser.FirstOrDefaultAsync(n => n.Id == model.Id);
                    
                    user.UserId = model.UserId;
                    user.UserEmail = model.UserId + "@first-resources.com";
                    user.ModifyBy = model.ModifyBy;
                    user.ModifyDate = model.ModifyDate;

                    _context.Entry(user).State = EntityState.Modified;
                    await _context.SaveChangesAsync();
                    #endregion

                    #region Employee
                    var emp = await _context.MasterDataEmployee.FirstOrDefaultAsync(n => n.MasterDataUserId == model.Id);

                    emp.EmployeeName = model.UserName;
                    emp.Email = model.UserId + "@first-resources.com";
                    emp.RegionOffice = model.Region;
                    emp.ModifyBy = model.ModifyBy;
                    emp.ModifyDate = model.ModifyDate;

                    _context.Entry(emp).State = EntityState.Modified;
                    await _context.SaveChangesAsync();
                    #endregion

                    dbcxtransaction.Commit();

                    response = ResponseConstant.UPDATE_SUCCESS;
                }
                catch (Exception ex)
                {
                    response.Message = ex.Message.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }
            return response;
        }

        public async Task<BaseResponse> DeleteUserAdmin(int id)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var user = await _context.MasterDataUser.FirstOrDefaultAsync(n => n.Id == id);

                    if (user.UserLevel == 3)
                    {
                        _context.Remove(user);
                        await _context.SaveChangesAsync();

                        var emp = await _context.MasterDataEmployee.FirstOrDefaultAsync(n => n.MasterDataUserId == id);

                        _context.Remove(emp);
                        await _context.SaveChangesAsync();

                        response.Code = (int)HttpStatusCode.OK;
                        response.Status = HttpStatusCode.OK.ToString();
                        response.Message = "Account " + user.UserId + " has been deleted successfully";
                    }
                    else 
                    {
                        response.Code = (int)HttpStatusCode.BadRequest;
                        response.Status = HttpStatusCode.BadRequest.ToString();
                        response.Message = "Cannot delete account Super Admin";
                    }

                    dbcxtransaction.Commit();
                }
                catch (Exception ex)
                {
                    response.Message = ex.Message.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }
            return response;
        }
    }
}