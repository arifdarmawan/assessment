﻿using Microsoft.EntityFrameworkCore;
using SelfAssessment.Model.Entities;
using SelfAssessment.Model.Models;
using SelfAssessment.Model.Models.User;
using SelfAssessment.Repository.Abstract;
using SelfAssessment.Repository.Helper;
using System;
using System.DirectoryServices.AccountManagement;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace SelfAssessment.Repository.Concrete
{
    public class UserRepository : IUserRepository
    {
        readonly SelfAssessmentContext _context;
        public UserRepository(SelfAssessmentContext context)
        {
            _context = context;
        }

        public async Task<BaseResponse> Insert(LoginViewModel data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    if (_context.MasterDataUser.Where(n => n.Password == data.Password).Count() == 0)
                    {
                        MasterDataUser newUser = new MasterDataUser
                        {
                            UserId = data.UserId,
                            Password = data.Password,
                            UserEmail = "admin@first-resources.com",
                            UserLevel = 1,
                            CreatedBy = "System",
                            CreatedDate = DateTime.Now,
                            ModifyBy = "System",
                            ModifyDate = DateTime.Now,
                            IsActive = true,
                            RegistrationNumber = "CTC" + String.Format(CultureInfo.InvariantCulture, "{0:0000000}", await _context.MasterDataUser.MaxAsync(n => n.Id) + 1)
                        };

                        await _context.MasterDataUser.AddAsync(newUser);
                        await _context.SaveChangesAsync();

                        dbcxtransaction.Commit();

                        response = ResponseConstant.SAVE_SUCCESS;
                    }
                    else
                    {
                        response.Code = (int)HttpStatusCode.BadRequest;
                        response.Status = HttpStatusCode.BadRequest.ToString();
                        response.Message = "Email already exist.";

                        dbcxtransaction.Rollback();
                    }
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }

        public async Task<BaseResponse> GetUserByEmail(string email)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from employee in _context.MasterDataEmployee
                                    join user in _context.MasterDataUser
                                    on employee.MasterDataUserId equals user.Id
                                    where user.UserEmail == email
                                    select employee).FirstOrDefaultAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetUserByEmailOrUserId(string userid)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from user in _context.MasterDataUser
                                    where user.UserId == userid
                                    select user).FirstOrDefaultAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;
                response.Message = "User Found";

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetUserById(int id)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from user in _context.MasterDataUser
                                    where user.Id == id
                                    select user).FirstOrDefaultAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;
                response.Message = "User Found";

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> InsertUserFromAd(LoginViewModel model)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    //var domains = await _context.MasterDataDomain.ToListAsync();
                    var domains = await _context.MasterDataDomain.FirstOrDefaultAsync();

                    //foreach (var domain in domains)
                    //{
                        try
                        {
                            var domainContext = new PrincipalContext(ContextType.Domain, domains.DomainLink, model.UserId, model.Password);

                            var userAD = UserPrincipal.FindByIdentity(domainContext, IdentityType.SamAccountName, model.UserId);

                            if (userAD != null)
                            {
                                #region USER
                                MasterDataUser newUser = new MasterDataUser
                                {
                                    UserId = model.UserId,
                                    UserEmail = userAD.EmailAddress,
                                    UserLevel = 2,
                                    CreatedBy = userAD.GivenName + " " + userAD.Surname,
                                    CreatedDate = DateTime.Now,
                                    ModifyBy = userAD.GivenName + " " + userAD.Surname,
                                    ModifyDate = DateTime.Now,
                                    IsActive = true,
                                    Domain = domains.DomainLink
                                };

                                await _context.MasterDataUser.AddAsync(newUser);
                                await _context.SaveChangesAsync();
                                #endregion

                                #region EMPLOYEE
                                var datauser = await _context.MasterDataEmployee.FirstOrDefaultAsync(n => n.MasterDataUserId == newUser.Id);

                                if (datauser == null)
                                {
                                    MasterDataEmployee employee = new MasterDataEmployee
                                    {
                                        EmployeeName = userAD.GivenName + " " + userAD.Surname,
                                        MasterDataUserId = newUser.Id,
                                        MasterDataKelurahanId = 0,
                                        Gender = 0,
                                        Email = userAD.EmailAddress,
                                        RegionOffice = await GlobalHelper.GetProperty(userAD, "physicalDeliveryOfficeName"),
                                        Jabatan = await GlobalHelper.GetProperty(userAD, "title"),
                                        Divisi = await GlobalHelper.GetProperty(userAD, "department"),
                                        ExtensionNumber = userAD.VoiceTelephoneNumber,
                                        IsActive = true,
                                        CreatedBy = model.UserId,
                                        CreatedDate = DateTime.Now,
                                        ModifyBy = model.UserId,
                                        ModifyDate = DateTime.Now
                                    };

                                    var getNIK = await _context.MasterDataEmployeeDummy.FirstOrDefaultAsync(n => n.Email == userAD.EmailAddress);
                                    
                                    if (getNIK != null) 
                                    {
                                        employee.Nik = getNIK.Nik;
                                    }

                                    await _context.MasterDataEmployee.AddAsync(employee);
                                    await _context.SaveChangesAsync();
                                }
                                #endregion

                                dbcxtransaction.Commit();

                                response.Code = (int)HttpStatusCode.OK;
                                response.Status = HttpStatusCode.OK.ToString();
                                response.Message = "User successfull inserted.";
                                response.Data = newUser;
                            }
                        }
                        catch (Exception ex)
                        {
                            response.Message = ex.Message;
                        }
                    //}
                }
                catch (Exception ex)
                {
                    dbcxtransaction.Rollback();

                    if (ex.Message.Contains("The user name or password is incorrect."))
                    {
                        response.Code = (int)CustomStatusCode.UserAdNotRegistered;
                        response.Message = "Username not registered. Please contact administrator.";
                    }
                    else
                    {
                        response.Message = ex.ToString();
                        response.Code = (int)HttpStatusCode.InternalServerError;
                        response.Status = HttpStatusCode.InternalServerError.ToString();
                    }
                }
            }

            return response;
        }

        public async Task<BaseResponse> UpdateUserFromAd(ActiveDirectoryUserModel data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var user = await _context.MasterDataUser.FirstOrDefaultAsync(n => n.UserId == data.UserId);

                    using (var domainContext = new PrincipalContext(ContextType.Domain, user.Domain, user.UserId, data.Password))
                    {
                        using (var userAD = UserPrincipal.FindByIdentity(domainContext, IdentityType.SamAccountName, user.UserId))
                        {
                            if (userAD != null)
                            {
                                var employee = await _context.MasterDataEmployee.FirstOrDefaultAsync(n => n.MasterDataUserId == user.Id);

                                if (employee != null) 
                                {
                                    if (employee.Nik == null)
                                    {
                                        var getNIK = await _context.MasterDataEmployeeDummy.FirstOrDefaultAsync(n => n.Email == userAD.EmailAddress);
                                        if (getNIK != null) 
                                        {
                                            employee.Nik = getNIK.Nik;
                                        }
                                        
                                    }
                                    employee.EmployeeName = userAD.GivenName + " " + userAD.Surname;
                                    employee.Email = userAD.EmailAddress;
                                    employee.RegionOffice = await GlobalHelper.GetProperty(userAD, "physicalDeliveryOfficeName");
                                    employee.Jabatan = await GlobalHelper.GetProperty(userAD, "title");
                                    employee.Divisi = await GlobalHelper.GetProperty(userAD, "department");
                                    employee.ExtensionNumber = userAD.VoiceTelephoneNumber;
                                    employee.ModifyBy = data.UserId;
                                    employee.ModifyDate = DateTime.Now;
                                }
                                else
                                {
                                    response = ResponseConstant.NO_RESULT;
                                }

                                _context.Entry(employee).State = EntityState.Modified;
                                await _context.SaveChangesAsync();

                                dbcxtransaction.Commit();

                                response.Code = (int)HttpStatusCode.OK;
                                response.Status = HttpStatusCode.OK.ToString();
                                response.Message = "User Exist.";
                                response.Data = user;
                            }
                            else
                            {
                                response = ResponseConstant.NO_RESULT;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    dbcxtransaction.Rollback();

                    if (ex.Message.Contains("The user name or password is incorrect."))
                    {
                        response.Code = (int)HttpStatusCode.BadRequest;
                        response.Status = HttpStatusCode.BadRequest.ToString();
                        response.Message = ex.Message;
                    }
                    else
                    {
                        response.Code = (int)HttpStatusCode.InternalServerError;
                        response.Status = HttpStatusCode.InternalServerError.ToString();
                        response.Message = ex.Message;
                    }
                }
            }

            return response;
        }

        public async Task<BaseResponse> GetDataUserAD(int userid)
        {
            BaseResponse response = new BaseResponse();
            try
            {
                var result = await (from emp in _context.MasterDataEmployee
                                    where emp.MasterDataUserId == userid
                                    select new
                                    {
                                        emp.MasterDataUserId,
                                        NIK = emp.Nik,
                                        emp.EmployeeName,
                                        emp.Gender,
                                        emp.MasterDataKelurahanId,
                                        emp.Address,
                                        emp.Email
                                    }).FirstOrDefaultAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "Data not Found";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetEmployeeByUserId(int userid)
        {
            BaseResponse response = new BaseResponse();
            try 
            {
                var result = await (from emp in _context.MasterDataEmployee
                                    join kel in _context.MasterDataKelurahan
                                        on emp.MasterDataKelurahanId equals kel.Id
                                    where emp.MasterDataUserId == userid
                                    select new
                                    {
                                        emp.MasterDataUserId,
                                        NIK = emp.Nik,
                                        emp.EmployeeName,
                                        emp.Gender,
                                        emp.MasterDataKelurahanId,
                                        Kota = kel.KotaKabupaten,
                                        kel.Kecamatan,
                                        kel.Kelurahan,
                                        kel.Kodepos,
                                        emp.Address,
                                        emp.Email
                                    }).FirstOrDefaultAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "Data not Found";
                }

                response.Data = result;
            }
            catch (Exception ex) 
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> UpdateEmployee(EmployeeModel model)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var data = await _context.MasterDataEmployee.FirstOrDefaultAsync(n => n.MasterDataUserId == model.MasterDataUserId);

                    if (data != null)
                    {
                        data.Address = model.Address;
                        data.MasterDataKelurahanId = model.MasterDataKelurahanId;
                        data.Gender = model.Gender;
                        data.ModifyBy = model.ModifyBy;
                        data.ModifyDate = model.ModifyDate;
                    }
                    else
                    {
                        response = ResponseConstant.NO_RESULT;
                    }

                    _context.Entry(data).State = EntityState.Modified;
                    await _context.SaveChangesAsync();

                    dbcxtransaction.Commit();

                    response.Code = (int)HttpStatusCode.OK;
                    response.Status = HttpStatusCode.OK.ToString();
                    response.Message = "The information was updated successfully";

                }
                catch(Exception ex)
                {
                    dbcxtransaction.Rollback();
                    
                    response.Code = (int)HttpStatusCode.BadRequest;
                    response.Status = HttpStatusCode.BadRequest.ToString();
                    response.Message = ex.Message;
                }
            }

            return response;
        }
    }
}