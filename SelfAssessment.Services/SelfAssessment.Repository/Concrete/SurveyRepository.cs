﻿using Microsoft.EntityFrameworkCore;
using QRCoder;
using SelfAssessment.Model.Entities;
using SelfAssessment.Model.Models;
using SelfAssessment.Model.Models.Survey;
using SelfAssessment.Repository.Abstract;
using SelfAssessment.Repository.Helper;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace SelfAssessment.Repository.Concrete
{
    public class SurveyRepository : ISurveyRepository
    {
        readonly SelfAssessmentContext _context;
        public SurveyRepository(SelfAssessmentContext context)
        {
            _context = context;
        }

        public async Task<BaseResponse> GetPersonalDatabyUserId(int userid)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from user in _context.MasterDataUser
                                    join employee in _context.MasterDataEmployee
                                        on user.Id equals employee.MasterDataUserId
                                    where user.Id == userid
                                    select new
                                    {
                                        employee.MasterDataUserId,
                                        NIK = employee.Nik,
                                        Name = employee.EmployeeName,
                                        employee.Email,
                                        employee.Address,
                                        employee.MasterDataKelurahanId,
                                        employee.Gender
                                    }).FirstOrDefaultAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch(Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetDataSurveybyDateNow(int employeeid)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await _context.MasterDataSurvey.FirstOrDefaultAsync(n => n.MasterDataUserId == employeeid && n.CreatedDate.Date >= DateTime.Now.Date);

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Anda Sudah Mengisi Survey Hari Ini";
                }
                else
                {
                    response.Message = "Silakah Mengisi Self Assessment Anda Hari Ini";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> InsertDataSurvey(DataSurveyModel model)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var datauser = await _context.MasterDataEmployee.FirstOrDefaultAsync(n => n.MasterDataUserId == model.MasterDataUserId);

                    #region Data Survey
                    MasterDataSurvey dataSurvey = new MasterDataSurvey
                    {
                        MasterDataUserId = model.MasterDataUserId,
                        Question1 = model.Question1,
                        Question2 = model.Question2,
                        Question3 = model.Question3,
                        Question4 = model.Question4,
                        Question5 = model.Question5,
                        Question6 = model.Question6,
                        Question7 = model.Question7,
                        Question8 = model.Question8,
                        Question9 = model.Question9,
                        Question10 = model.Question10,
                        Question11 = model.Question11,
                        Question12 = model.Question12,
                        Question13 = model.Question13,
                        Question14 = model.Question14,
                        Question15 = model.Question15,
                        Question16 = model.Question16,
                        Question17 = model.Question17,
                        Question18 = model.Question18,
                        IsApproved = model.IsApproved,
                        CreatedBy = model.CreatedBy,
                        CreatedDate = model.CreatedDate,
                        ModifyBy = model.ModifyBy,
                        ModifyDate = model.ModifyDate,
                        IsDeleted = false
                    };

                    await _context.MasterDataSurvey.AddAsync(dataSurvey);
                    await _context.SaveChangesAsync();
                    #endregion

                    #region Result
                    int result_color = 0;
                    var data_qrcode = Guid.NewGuid().ToString();

                    if (model.Region == "Jakarta")
                    {
                        result_color = ResultCode.ResultCodeJakarta(dataSurvey);
                    } 
                    else if (model.Region == "Balikpapan") 
                    {
                        result_color = ResultCode.ResultCodeBalikpapan(dataSurvey);
                    }

                    string img_result = null;
                    using (MemoryStream ms = new MemoryStream())
                    {
                        QRCodeGenerator _qrCode = new QRCodeGenerator();
                        QRCodeData _qrCodeData = _qrCode.CreateQrCode(data_qrcode, QRCodeGenerator.ECCLevel.Q);
                        QRCode qrCode = new QRCode(_qrCodeData);
                        using (Bitmap oBitmap = qrCode.GetGraphic(20))
                        {
                            oBitmap.Save(ms, ImageFormat.Png);
                            img_result = Convert.ToBase64String(ms.ToArray());
                        }
                    }

                    MasterDataResult dataresult = new MasterDataResult
                    {
                        MasterDataUserId = model.MasterDataUserId,
                        Nama = datauser.EmployeeName,
                        ColorCode = result_color,
                        Images = img_result,
                        Qrcode = data_qrcode,
                        CreatedBy = model.CreatedBy,
                        CreatedDate = model.CreatedDate,
                        ModifyBy = model.ModifyBy,
                        ModifyDate = model.ModifyDate
                    };
                    await _context.MasterDataResult.AddAsync(dataresult);
                    await _context.SaveChangesAsync();
                    #endregion

                    #region Mapping Survey Question
                    MasterDataMappingSurveyQuestion mappingSurvey = new MasterDataMappingSurveyQuestion
                    {
                        MasterDataUserId = model.MasterDataUserId,
                        Qcode1 = model.Question1,
                        Qcode2 = model.Question2,
                        Qcode3 = model.Question3,
                        Qcode5 = model.Question5,
                        Qcode6 = model.Question6,
                        Qcode7 = model.Question7,
                        Qcode8 = model.Question8,
                        Qcode9 = model.Question9,
                        Qcode10 = model.Question10,
                        Qcode11 = model.Question11,
                        Qcode12 = model.Question12,
                        Qcode13 = model.Question13,
                        Qcode14 = model.Question14,
                        Qcode15 = model.Question15,
                        Qcode16 = model.Question16,
                        Qcode17 = model.Question17,
                        Qcode18 = model.Question18,
                        CreatedBy = model.CreatedBy,
                        CreatedDate = model.CreatedDate,
                        ModifyBy = model.ModifyBy,
                        ModifyDate = model.ModifyDate,
                        IsDeleted = false
                    };

                    await _context.MasterDataMappingSurveyQuestion.AddAsync(mappingSurvey);
                    await _context.SaveChangesAsync();
                    #endregion

                    response.Code = (int)HttpStatusCode.OK;
                    response.Status = HttpStatusCode.OK.ToString();
                    response.Message = "successfull insert data survey.!";
                    response.Data = dataresult;

                    dbcxtransaction.Commit();
                }
                catch (Exception ex)
                {
                    response.Message = ex.Message.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }
            return response;
        }

        public async Task<BaseResponse> GetQrCode(int userId)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var res = await _context.MasterDataResult.FirstOrDefaultAsync(n => n.MasterDataUserId == userId && n.CreatedDate.Date == DateTime.Now.Date);

                if (res != null) 
                {
                    response.Code = res.ColorCode;
                    response.Status = HttpStatusCode.OK.ToString();
                    response.Data = res.Images;
                }
            }
            catch(Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }
    }
}