﻿using Microsoft.EntityFrameworkCore;
using SelfAssessment.Model.Entities;
using SelfAssessment.Model.Models;
using SelfAssessment.Repository.Abstract;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace SelfAssessment.Repository.Concrete
{
    public class KelurahanRepository : IKelurahanRepository
    {
        readonly SelfAssessmentContext _context;
        public KelurahanRepository(SelfAssessmentContext context)
        {
            _context = context;
        }

        public async Task<BaseResponse> GetDataKelurahan()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from kelurahan in _context.MasterDataKelurahan
                                    select new
                                    {
                                        kelurahan.Id,
                                        kelurahan.Kelurahan,
                                        kota = kelurahan.KotaKabupaten,
                                        kelurahan.Kodepos
                                    }).AsNoTracking().ToListAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetDataKelurahanByRegionCode(string region)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from kelurahan in _context.MasterDataKelurahan
                                    where kelurahan.RegionCode == region
                                    select new
                                    {
                                        kelurahan.Id,
                                        kelurahan.Kelurahan,
                                        kota = kelurahan.KotaKabupaten,
                                        kelurahan.Kecamatan,
                                        kelurahan.Kodepos
                                    }).AsNoTracking().ToListAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }

        public async Task<BaseResponse> GetDataKelurahanById(int id)
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from kelurahan in _context.MasterDataKelurahan
                                    where kelurahan.Id == id
                                    select new
                                    {
                                        kelurahan.Kecamatan,
                                        kota = kelurahan.KotaKabupaten,
                                        kelurahan.Kodepos
                                    }).FirstOrDefaultAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }
    }
}