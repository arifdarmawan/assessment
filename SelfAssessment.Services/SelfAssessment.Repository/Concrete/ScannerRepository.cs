﻿using Microsoft.EntityFrameworkCore;
using SelfAssessment.Model.Entities;
using SelfAssessment.Model.Models;
using SelfAssessment.Model.Models.Reader;
using SelfAssessment.Repository.Abstract;
using System;
using System.Net;
using System.Threading.Tasks;

namespace SelfAssessment.Repository.Concrete
{
    public class ScannerRepository : IScannerRepository
    {
        readonly SelfAssessmentContext _context;
        public ScannerRepository(SelfAssessmentContext context)
        {
            _context = context;
        }

        public async Task<BaseResponse> UpdateDataResultAfterScanQRCode(ReaderModel data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var result = await _context.MasterDataResult.FirstOrDefaultAsync(
                        n => n.Qrcode == data.QRCode && n.CreatedDate.Date == DateTime.Now.Date);

                    if (result != null)
                    {
                        if (result.JamMasuk == null)
                        {
                            result.JamMasuk = data.Time;
                            result.ModifyBy = "System";
                            result.ModifyDate = DateTime.Now;

                            response.Message = "IN";

                        }
                        else
                        {
                            result.JamPulang = data.Time;
                            result.ModifyBy = "System";
                            result.ModifyDate = DateTime.Now;

                            response.Message = "OUT";
                        }

                        response.Data = result.ColorCode;
                        response.Status = HttpStatusCode.OK.ToString();
                        response.Code = (int)HttpStatusCode.OK;
                    }
                    else
                    {
                        response.Message = "expired";
                    }

                    _context.Entry(result).State = EntityState.Modified;
                    await _context.SaveChangesAsync();

                    dbcxtransaction.Commit();
                }
                catch (Exception ex)
                {
                    response.Status = HttpStatusCode.InternalServerError.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Message = ex.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }
    }
}