﻿using SelfAssessment.Model.Entities;
using SelfAssessment.Model.Models;
using SelfAssessment.Repository.Abstract;
using SelfAssessment.Repository.Helper;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SelfAssessment.Repository.Concrete
{
    public class FileRepository : IFileRepository
    {
        readonly SelfAssessmentContext _context;
        public FileRepository(SelfAssessmentContext context)
        {
            _context = context;
        }
        public async Task<BaseResponse> DownloadFile(string fileName)
        {
            BaseResponse response = new BaseResponse();
            ImageBaseResponse imageResponse = new ImageBaseResponse();

            try
            {
                var path = Path.Combine(Directory.GetCurrentDirectory(), "FileUploads", fileName);

                var memory = new MemoryStream();
                using (var stream = new FileStream(path, FileMode.Open))
                {
                    await stream.CopyToAsync(memory);
                }
                memory.Position = 0;

                imageResponse.Status = HttpStatusCode.OK.ToString();
                imageResponse.Code = (int)HttpStatusCode.OK;
                imageResponse.file = memory;
                imageResponse.filePath.Add(GlobalHelper.GetContentType(path).Result);
                imageResponse.fileNames.Add(Path.GetFileName(path));

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;
                response.Message = "Retrieve File Success.";
                response.Data = imageResponse;
            }
            catch (Exception ex)
            {
                imageResponse.Status = HttpStatusCode.InternalServerError.ToString();
                imageResponse.Code = (int)HttpStatusCode.InternalServerError;
                imageResponse.Message = ex.ToString();

                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = "Failed to Retrieve File.";
            }

            return response;
        }
    }
}
