﻿using SelfAssessment.Model.Entities;
using SelfAssessment.Model.Models;
using System;
using System.Net;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using SelfAssessment.Repository.Abstract;

namespace SelfAssessment.Repository.Concrete
{
    public class RegionRepository : IRegionRepository
    {
        readonly SelfAssessmentContext _context;
        public RegionRepository(SelfAssessmentContext context)
        {
            _context = context;
        }

        public async Task<BaseResponse> GetDataRegion()
        {
            BaseResponse response = new BaseResponse();

            try
            {
                var result = await (from reg in _context.MasterDataRegion
                                    select new
                                    {
                                        RegionId = reg.Region,
                                        reg.Region
                                    }).AsNoTracking().Distinct().ToListAsync();

                response.Status = HttpStatusCode.OK.ToString();
                response.Code = (int)HttpStatusCode.OK;

                if (result != null)
                {
                    response.Message = "Retrieve data success";
                }
                else
                {
                    response.Message = "No result";
                }

                response.Data = result;
            }
            catch (Exception ex)
            {
                response.Status = HttpStatusCode.InternalServerError.ToString();
                response.Code = (int)HttpStatusCode.InternalServerError;
                response.Message = ex.ToString();
            }

            return response;
        }
    }
}
