﻿using Microsoft.Extensions.Options;
using SelfAssessment.Model.Models.User;
using SelfAssessment.Repository.Abstract;
using SelfAssessment.Repository.Helper;
using System.Collections.Generic;

namespace SelfAssessment.Repository.Concrete
{
    public class UserService : IUserService
    {
        private List<UserModel> _users = new List<UserModel>
        {
            new UserModel { Id = 1, FirstName = "user", LastName = "data", Username = "sak.users", Password = "123" }
        };

        private readonly AppSettings _appSettings;

        public UserService(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }

        public IEnumerable<UserModel> GetAll()
        {
            return _users.WithoutPasswords();
        }
    }
}
