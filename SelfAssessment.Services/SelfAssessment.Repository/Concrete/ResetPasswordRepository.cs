﻿using Microsoft.EntityFrameworkCore;
using SelfAssessment.Model.Entities;
using SelfAssessment.Model.Models;
using SelfAssessment.Model.Models.Admin;
using SelfAssessment.Repository.Abstract;
using System;
using System.Net;
using System.Threading.Tasks;

namespace SelfAssessment.Repository.Concrete
{
    public class ResetPasswordRepository : IResetPasswordRepository
    {
        readonly SelfAssessmentContext _context;
        public ResetPasswordRepository(SelfAssessmentContext context)
        {
            _context = context;
        }

        public async Task<BaseResponse> SetNewPassword(NewPasswordModel data)
        {
            BaseResponse response = new BaseResponse();

            using (var dbcxtransaction = await _context.Database.BeginTransactionAsync())
            {
                try
                {
                    var user = await _context.MasterDataUser.FirstOrDefaultAsync(n => n.UserId == data.UserId);

                    if (user.Password == data.OldPassword)
                    {
                        user.Password = data.ConfirmPassword;
                        user.ModifyBy = data.ModifyBy;
                        user.ModifyDate = data.ModifyDate;

                        _context.Entry(user).State = EntityState.Modified;

                        await _context.SaveChangesAsync();

                        dbcxtransaction.Commit();

                        response.Message = "Your password has been changed successfully";
                        response.Code = (int)HttpStatusCode.OK;
                        response.Status = HttpStatusCode.OK.ToString();
                    }
                    else 
                    {
                        response.Code = (int)HttpStatusCode.BadRequest;
                        response.Status = HttpStatusCode.BadRequest.ToString();
                        response.Message = "The old password you have entered is incorrect";
                    }

                    return response;
                        
                }
                catch (Exception ex)
                {
                    response.Message = ex.ToString();
                    response.Code = (int)HttpStatusCode.InternalServerError;
                    response.Status = HttpStatusCode.InternalServerError.ToString();

                    dbcxtransaction.Rollback();
                }
            }

            return response;
        }
    }
}
