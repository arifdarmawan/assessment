﻿using SelfAssessment.Model.Entities;
using SelfAssessment.Repository.Concrete;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SelfAssessment.Repository.Abstract;

namespace SelfAssessment.Services
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        readonly string SelfAssessmentAllowCORS = "_selfAssessmentAllowCORS";
        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc(options =>
            {
                options.EnableEndpointRouting = false;
            });

            string[] domains = new string[] {
                Configuration.GetValue<string>("Settings:SelfAssessmentDomain"),
                Configuration.GetValue<string>("Settings:ScannerQRDomain")
            };

            services.AddCors(options =>
            {
                options.AddPolicy(SelfAssessmentAllowCORS, builder =>
                {
                    builder.WithOrigins(domains);
                });
            });

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            RegisterDependencyInjection(services);

            var connection = Configuration.GetValue<string>("Settings:ConnectionString");

            // PostgreSQL
            if (!string.IsNullOrWhiteSpace(connection))
            {
                services.AddEntityFrameworkNpgsql().AddDbContext<SelfAssessmentContext>(opts =>
                {
                    opts.UseNpgsql(connection);
                });
            }

            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v2", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Title = "Self Assessment Services",
                    Version = "v2",
                    Description = "Application Services Library | Self Assessment | @first-resources.com",
                });
            });
        }

        private void RegisterDependencyInjection(IServiceCollection services)
        {
            services.AddSingleton(Configuration);
            services.AddScoped(typeof(IAdminRepository), typeof(AdminRepository));
            services.AddScoped(typeof(IDomainRepository), typeof(DomainRepository));
            services.AddScoped(typeof(IKelurahanRepository), typeof(KelurahanRepository));
            services.AddScoped(typeof(IRegionRepository), typeof(RegionRepository));
            services.AddScoped(typeof(IResetPasswordRepository), typeof(ResetPasswordRepository));
            services.AddScoped(typeof(IScannerRepository), typeof(ScannerRepository));
            services.AddScoped(typeof(ISurveyRepository), typeof(SurveyRepository));
            services.AddScoped(typeof(IUserRepository), typeof(UserRepository));
            services.AddScoped(typeof(IUserService), typeof(UserService));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v2/swagger.json", " Self Assessment Services V2");
                c.DefaultModelExpandDepth(0);
                c.DefaultModelsExpandDepth(-1);
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            var option = new RewriteOptions();
            option.AddRedirect("^$", "swagger");
            app.UseRewriter(option);
            app.UseStaticFiles();
            app.UseMvc();
        }
    }
}