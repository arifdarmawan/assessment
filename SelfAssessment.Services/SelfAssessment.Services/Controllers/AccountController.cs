﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SelfAssessment.Model.Models;
using SelfAssessment.Model.Models.User;
using SelfAssessment.Repository.Abstract;

namespace SelfAssessment.Services.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IUserRepository _MsUserRepository;
        private readonly IDomainRepository _MsDomainRepository;
        private readonly IResetPasswordRepository _ResetPasswordRepository;
        public AccountController(
            IUserRepository MasterDataUserRepository,
            IDomainRepository MasterDataDomainRepository,
            IResetPasswordRepository MasterDataResetPasswordRepository)
        {
            _MsUserRepository = MasterDataUserRepository;
            _MsDomainRepository = MasterDataDomainRepository;
            _ResetPasswordRepository = MasterDataResetPasswordRepository;
        }

        [HttpGet("GetUserByEmail")]
        public async Task<BaseResponse> GetUserByEmail(string email)
        {
            return await _MsUserRepository.GetUserByEmail(email);
        }

        [HttpGet("GetUserByEmailOrUserId")]
        public async Task<BaseResponse> GetUserByEmailOrUserId(string userid)
        {
            return await _MsUserRepository.GetUserByEmailOrUserId(userid);
        }

        [HttpGet("GetUserById")]
        public async Task<BaseResponse> GetUserById(int id)
        {
            return await _MsUserRepository.GetUserById(id);
        }

        [HttpPost("InsertUser")]
        public async Task<BaseResponse> InsertUser([FromBody] LoginViewModel data)
        {
            return await _MsUserRepository.Insert(data);
        }

        [HttpPost("InsertUserFromAD")]
        public async Task<BaseResponse> InsertUserFromAD([FromBody] LoginViewModel model)
        {
            return await _MsUserRepository.InsertUserFromAd(model);
        }

        [HttpPost("UpdateUserFromAD")]
        public async Task<BaseResponse> UpdateUserFromAD([FromBody] ActiveDirectoryUserModel data)
        {
            return await _MsUserRepository.UpdateUserFromAd(data);
        }

        [HttpGet("GetEmployeeByUserId")]
        public async Task<BaseResponse> GetEmployeeByUserId(int userid)
        {
            return await _MsUserRepository.GetEmployeeByUserId(userid);
        }

        [HttpGet("GetDataUserAD")]
        public async Task<BaseResponse> GetDataUserAD(int userid)
        {
            return await _MsUserRepository.GetDataUserAD(userid);
        }

        [HttpPost("UpdateEmployee")]
        public async Task<BaseResponse> UpdateEmployee([FromBody] EmployeeModel model)
        {
            return await _MsUserRepository.UpdateEmployee(model);
        }
    }
}