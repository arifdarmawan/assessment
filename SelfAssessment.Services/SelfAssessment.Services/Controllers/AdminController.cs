﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SelfAssessment.Model.Models;
using SelfAssessment.Model.Models.Admin;
using SelfAssessment.Repository.Abstract;

namespace SelfAssessment.Services.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly IAdminRepository _MsAdminRepository;
        private readonly IResetPasswordRepository _ResetPasswordRepository;

        public AdminController(IAdminRepository AdminRepository, IResetPasswordRepository ResetPasswordRepository)
        {
            _MsAdminRepository = AdminRepository;
            _ResetPasswordRepository = ResetPasswordRepository;
        }

        [HttpGet("getTotalResultUserbyColorCode")]
        public async Task<BaseResponse> getTotalResultUserbyColorCode(string userlevel, string region)
        {
            return await _MsAdminRepository.GetTotalResultUserbyColorCode(userlevel, region);
        }

        [HttpGet("GetResultUSerSurveybyDateNow")]
        public async Task<BaseResponse> GetResultUSerSurveybyDateNow(string userlevel, string region)
        {
            return await _MsAdminRepository.GetResultUSerSurveybyDateNow(userlevel, region);
        }

        [HttpGet("GetResultSurveyByStartAndEndDate")]
        public async Task<BaseResponse> GetResultSurveyByStartAndEndDate(DateTime startDate, DateTime endDate, string userlevel, string region)
        {
            return await _MsAdminRepository.GetResultSurveyByStartAndEndDate(startDate, endDate, userlevel, region);
        }

        [HttpGet("GetDetailResultSurvey")]
        public async Task<BaseResponse> GetDetailResultSurvey(int userId, DateTime date)
        {
            return await _MsAdminRepository.GetDetailResultSurvey(userId, date);
        }

        [HttpGet("GetDataUserAdmin")]
        public async Task<BaseResponse> GetDataUserAdmin()
        {
            return await _MsAdminRepository.GetDataUserAdmin();
        }

        [HttpPost("InsertUserAdmin")]
        public async Task<BaseResponse> InsertUserAdmin(UserAdminModel model)
        {
            return await _MsAdminRepository.InsertUserAdmin(model);
        }

        [HttpPost("UpdateUserAdmin")]
        public async Task<BaseResponse> UpdateUserAdmin(UserAdminModel model)
        {
            return await _MsAdminRepository.UpdateUserAdmin(model);
        }

        [HttpGet("DeleteUserAdmin")]
        public async Task<BaseResponse> DeleteUserAdmin(int id)
        {
            return await _MsAdminRepository.DeleteUserAdmin(id);
        }

        [HttpPost("SetNewPassword")]
        public async Task<BaseResponse> SetNewPassword([FromBody] NewPasswordModel data)
        {
            return await _ResetPasswordRepository.SetNewPassword(data);
        }
    }
}