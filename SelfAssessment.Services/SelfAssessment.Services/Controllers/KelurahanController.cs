﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SelfAssessment.Model.Models;
using SelfAssessment.Repository.Abstract;

namespace SelfAssessment.Services.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KelurahanController : ControllerBase
    {
        private readonly IKelurahanRepository _MsKelurahanRepository;

        public KelurahanController(IKelurahanRepository KelurahanRepository)
        {
            _MsKelurahanRepository = KelurahanRepository;
        }

        [HttpGet("GetDataKelurahan")]
        public async Task<BaseResponse> GetDataKelurahan()
        {
            return await _MsKelurahanRepository.GetDataKelurahan();
        }

        [HttpGet("GetDataKelurahanByRegionCode")]
        public async Task<BaseResponse> GetDataKelurahanByRegionCode(string region)
        {
            return await _MsKelurahanRepository.GetDataKelurahanByRegionCode(region);
        }

        [HttpGet("GetDataKelurahanById")]
        public async Task<BaseResponse> GetDataKelurahanById(int id)
        {
            return await _MsKelurahanRepository.GetDataKelurahanById(id);
        }
    }
}