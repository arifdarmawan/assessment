﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SelfAssessment.Model.Models;
using SelfAssessment.Repository.Abstract;

namespace SelfAssessment.Services.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RegionController : ControllerBase
    {
        private readonly IRegionRepository _MsRegionRepository;

        public RegionController(IRegionRepository RegionRepository)
        {
            _MsRegionRepository = RegionRepository;
        }

        [HttpGet("GetDataRegion")]
        public async Task<BaseResponse> GetDataRegion()
        {
            return await _MsRegionRepository.GetDataRegion();
        }
    }
}