﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SelfAssessment.Model.Models;
using SelfAssessment.Model.Models.Reader;
using SelfAssessment.Repository.Abstract;

namespace SelfAssessment.Services.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ScannerController : ControllerBase
    {
        private readonly IScannerRepository _MsScannerRepository;
        public ScannerController(IScannerRepository scannerRepository)
        {
            _MsScannerRepository = scannerRepository;
        }

        [HttpPost("UpdateDataResultAfterScanQRCode")]
        public async Task<BaseResponse> UpdateDataResultAfterScanQRCode([FromBody] ReaderModel data)
        {
            return await _MsScannerRepository.UpdateDataResultAfterScanQRCode(data);
        }
    }
}