﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SelfAssessment.Model.Models;
using SelfAssessment.Model.Models.Survey;
using SelfAssessment.Repository.Abstract;

namespace SelfAssessment.Services.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SurveyController : ControllerBase
    {
        private readonly ISurveyRepository _MsSurveyRepository;
        public SurveyController(ISurveyRepository SurveyRepository)
        {
            _MsSurveyRepository = SurveyRepository;
        }

        [HttpGet("GetPersonalDatabyUserId")]
        public async Task<BaseResponse> GetPersonalDatabyUserId(int userid)
        {
            return await _MsSurveyRepository.GetPersonalDatabyUserId(userid);
        }

        [HttpGet("GetDataSurveybyDateNow")]
        public async Task<BaseResponse> GetDataSurveybyDateNow(int employeeid)
        {
            return await _MsSurveyRepository.GetDataSurveybyDateNow(employeeid);
        }

        [HttpGet("GetQrCode")]
        public async Task<BaseResponse> GetQrCode(int userId)
        {
            return await _MsSurveyRepository.GetQrCode(userId);
        }

        [HttpPost("InsertDataSurvey")]
        public async Task<BaseResponse> InsertDataSurvey(DataSurveyModel model) 
        {
            return await _MsSurveyRepository.InsertDataSurvey(model);
        }
    }
}
